BOMB BLOCKS 0.1a
================

Contents
--------
- 1.0 Overview
- 2.0 System Requirements
  - 2.1 Software
  - 2.2 Hardware
- 3.0 Gameplay
  - 3.1 Controls
  - 3.2 Configuration
  - 3.3 Scoring
  - 3.3.1 High Scores
- 4.0 Technical
  - 4.1 Troubleshooting
  - 4.2 Known Bugs
- 5.0 Support
  - 5.1 Website
  - 5.2 E-mail
- 6.0 Acknowledgements


1.0 OVERVIEW
------------
Bomb Blocks is a freeware game based on Tetris, but with bombs!

Conventional Tetris involves stacking blocks in a way in which you complete lines of tiles, which are then cleared and contribute to your score. Easy enough, and Bomb Blocks is no different in those stakes. However Bomb Blocks has an added dimension. There are three different types of bombs that can fall, doing all kinds of destruction and ruining those stacks that you so lovingly create. They are:
- Piledriver: Piledriver waits until it hits a tile on the stack then unleashes a tower of destruction, destroying all tiles in it's column.
- Leveller: Leveller is like piledriver, but destroys an entire row of tiles upon hitting the stack. This can be handy for clearing lines as all blocks above will collapse on top.
- Fatboy: Fatboy is a mischevious little devil. He's red and round and likes to make faces. But the thing with Fatboy is that you never know where he will go off. He has the ability to drop in front of the stack and blow up in a random position, destroying all tiles in his vicinity. You know when Fatboy is about to explode because he starts to glow and becomes alarmed at his impending destruction!



2.0 SYSTEM REQUIREMENTS
-----------------------

2.1 Software
------------
2.1.1 - Runtime
---------------
Java Runtime Environment 1.6:
* Available from http://www.java.com/ .

2.1.2 - Libraries
-----------------
Java Media Framework 2.1.1e:
* Available from https://www.oracle.com/technetwork/java/javase/download-142937.html
* Relative location from src folder is ../../../libs/JMF-2.1.1e

sqlite-jdbc 3.30.1.jar:
* Available from https://bitbucket.org/xerial/sqlite-jdbc/downloads/
* Relative location from src folder is ../../../libs/sqlite-jdbc


2.2 Hardware
------------
- Monitor minimum resolution 1024px x 768px.
- Keyboard and mouse controls supported.



3.0 GAMEPLAY
------------

3.1 Controls
------------
Left Arrow  / Move Mouse Left    - Move the block left
Right Arrow / Move Mouse RIght   - Move the block right
Up Arrow    / Left Mouse Button  - Rotate the block
Down Arrow  / Right Mouse Button - Move the block down

Buttons can be clicked by using the mouse, or pressing the first letter of the button text on the keyboard.

Note that when moving a block down you cannot move it over/behind a falling bomb, however you can move it over exploding tiles.


3.2 Configuration
-----------------
The following options can be configured in the Config menu, available from the Main Menu:
- Activate Bombs: This option can be turned off to allow you to play a classic Tetris-style game.
- Display Next Block: By default the next block is shown in a preview box to the bottom left of the gamefield. Turn this off to increase the challenge.
- Display Time Elapsed: Turn off the timer field shown to the right of the gamefield.
- Bomb Frequency: Determine how often the bombs fall. This can be set to every 20-30 seconds, 10-20 seconds, 5-10 seconds, 1-5 seconds, or set to constant for a greater challenge.
- Starting Level: The default starting level is 1. This is pretty slow but may be appropriate for beginners. Increase this for greater challenge, and go all the way up to 20 if you want to go nuts!


3.3 Scoring
-----------
- Four points scored for each tile dropped (four tiles in a block)
- Multiplier for the current level (e.g. level 4 x 7 tiles = 28 points)
- Clearing a row will result in an extra 10 points (1 for each tile) multiplied by the level (e.g. 3 rows x 10 tiles x level 6 = 180 points)
- Clearing four rows will start a streak. All point scores are multiplied by the streak value. The streak value is the number of rows cleared during a sequence where all four rows have been cleared (e.g. if four rows have been cleared three times the streak will be 12). Streak ends when there are less than four rows cleared during the next row clearance.


3.3.1 High Scores
---------------
If your score ranks in the top ten then at the end of the game you will be prompted to enter your name, after which it will be stored in the high score ladder.

The top ten high scores are saved and can be viewed in the ladder screen, accessible from the Main Menu.



4.0 TECHNICAL
-------------

4.1 Troubleshooting
-------------------
Q: I can't see all the text or graphics on the screen. What's wrong?
A: Ensure that your screen resolution is at least 1024px x 768px. Bomb Blocks requires screens of this resolution or higher to work properly.

Q: I can hear any audio on Linux.
A: See the Known Bugs regarding an audio bug with Java and Linux.


4.2 Known Bugs / Issues
-----------------------
- Audio does not work on Linux. This is due to an issue with PulseAudio and Java. It's possible that installing Java Runtime Environment 1.7 may fix this, but I have not yet tested this.
- In rare cases tiles will drop late after an explosion. I have yet to work out the cause of this.
- Blocks can be passed behind a falling bomb.
- Pressing 'Q' during the game exits without a prompt.
- Pressing 'Esc' anytime in the program exits without a prompt.



5.0 SUPPORT
-----------

5.1 Website
-----------
Offline


5.2 Email
---------
You can email support queries to the author at contact@andrewross.id.au . I'm also happy to hear if you have something nice to say about the game :-)



6.0 ACKNOWLEDGEMENTS
--------------------
Thanks to and hugs for Tracy Deichmann for all of her help and encouragement!


README Revision History
-----------------------
v1.0 - 2012/6/24 - Andrew Ross - Added sections 1-6
v1.1 - 2020/3/18 - Andrew Ross - Added required libraries to section 2.1.2
