//////////////////////////////////////////////////////////////////////////////////////
//
// FILE:		AbstractBlock.java 
// AUTHOR:		Andrew Ross
// DATE:		20/6/2012
// LAST UPDATE: 20/6/2012
// PURPOSE:		Base object for all the bombs appearing in the game.
//
/////////////////////////////////////////////////////////////////////////////////////


package blocks;

import java.util.Timer;
import java.util.TimerTask;

public abstract class AbstractBomb {

	protected static int bombX;
	protected static int bombY;

	protected transient String strBombType;
	protected transient int explodeY;
	protected transient int minExplodeX;
	protected transient int minExplodeY;
	protected transient int maxExplodeX;
	protected transient int maxExplodeY;
	private static Timer timer;
	private static boolean dropping;
	private static boolean paused;


	public AbstractBomb(final int startX, final int interval) {
		bombX = startX;
		bombY = 0;
		dropping = true;
		paused = false;

		timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				if (!paused) {
					bombY++;
				}
			}
		}, 1000, interval);
		
		setTilesToExplode();
	}
	
	public void drop() {
		bombY++;
	}
	
	public int getX() {
		return bombX;
	}

	public int getY() {
		return bombY;
	}
	
	public int getExplodeY() {
		return explodeY;
	}
	
	public int getMinExplodeX() {
		return minExplodeX;
	}
	
	public int getMaxExplodeX() {
		return maxExplodeX;
	}
	
	public int getMinExplodeY() {
		return minExplodeY;
	}
	
	public int getMaxExplodeY() {
		return maxExplodeY;
	}
	
	public String getBombType() {
		return strBombType;
	}
	
	public static boolean isBombDropping() {
		return dropping;
	}
	
	public static void stop() {
		dropping = false;
	}
	
	public void pause() {
		paused = true;
	}
	
	public void resume() {
		paused = false;
	}
	
	public boolean isPaused() {
		return paused;
	}
	
	protected void close() {
		timer.cancel();
		dropping = false;
		bombX = Constants.STACKHEIGHT + 1;
		bombY = Constants.STACKWIDTH + 1;
	}

	abstract public void setTilesToExplode();
	
	abstract public boolean explode(boolean resting);
}
