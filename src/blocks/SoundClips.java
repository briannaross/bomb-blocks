package blocks;

public final class SoundClips {
    private SoundClips() {
    	throw new AssertionError();
    }  // Prevents instantiation

	public static final AudioManager SND_RSTNG_BLCK = new AudioManager();
	public static final AudioManager SND_CLR_ROW = new AudioManager();
	public static final AudioManager SND_EXPLSN = new AudioManager();
}
