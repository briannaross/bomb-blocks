package blocks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;

public class ScreenAppEnd implements IScreen {

	private static final String STR_AUTHOR_YEAR = "� Andrew Ross 2012";

//	private Image imgBackground;
	private transient Image imgOuttro;
	private transient Graphics2D g2d;
	private transient DisplayManager scrMgr;
	private transient FontManager fntMgr;
	
	private transient int scrMidX;

	
	public ScreenAppEnd(final DisplayManager scrMgr, final FontManager fntMgr) {
		this.scrMgr = scrMgr;
		this.fntMgr = fntMgr;

		scrMidX = scrMgr.getWidth() / 2;
		
		loadImages();
	}
	
	@Override
	public void loadImages() {
		// TODO Auto-generated method stub
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "background.jpg")).getImage();
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "brickwall.jpg")).getImage();
		imgOuttro			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "outtro.jpg")).getImage();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		g2d = scrMgr.getGraphics();

		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, scrMgr.getWidth(), scrMgr.getHeight());		// Gamefield
//		g2d.drawImage(imgBackground, 0, 0, null);						// Brick wall
		g2d.drawImage(imgOuttro, scrMidX - 320, 100, null);
		g2d.setColor(Color.WHITE);
		g2d.drawString(STR_AUTHOR_YEAR, scrMidX - (fntMgr.getMetrics().stringWidth(STR_AUTHOR_YEAR) / 2), 700);

		g2d.dispose();
		scrMgr.update();
	}

	@Override
	public int checkMousePos(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}

}
