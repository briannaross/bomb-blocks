package blocks;

// The Leveller bomb will detonate on touching a tile on the stack and will set the blocks to explode
public class BombLeveller extends AbstractBomb {

	public BombLeveller(final int startX, final int interval) {
		// TODO Auto-generated constructor stub
		super(startX, interval);
		
		strBombType = "Leveller";
	}

	@Override
	public void setTilesToExplode() {
		// TODO Auto-generated method stub
		// Set minExplodeX and maxExplodeX here because we know the bomb will blow up all tiles on the X-axis of the row it detonates on.
		minExplodeX = 0;
		maxExplodeX = Constants.STACKWIDTH - 1;
	}

	@Override
	public boolean explode(final boolean resting) {
		// TODO Auto-generated method stub
		boolean explodeNow = false;

		if (resting) {
			// Set minExplodeY and maxExplodeY here because it's only here we know the row of the tiles that will be detonated.
			explodeY = bombY;
			minExplodeY = bombY;
			maxExplodeY = bombY;
			explodeNow = true;
		}

		return explodeNow;
		
	}

}
