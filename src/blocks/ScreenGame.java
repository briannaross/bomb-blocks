package blocks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;

public class ScreenGame implements IScreen {

	private DisplayManager scrMgr;
	private FontManager fntMgr;
	private Graphics2D g2d;

	private Image imgIntro;
	private Image imgBackground;
	private Image imgTileDarkBlue;
	private Image imgTileRed;
	private Image imgTileYellow;
	private Image imgTileLightBlue;
	private Image imgTileGreen;
	private Image imgTilePurple;
	private Image imgTileOrange;
	private Image imgFatboyBlowing;
	private Image imgArrowLeft;
	private Image imgArrowRight;
	private Image imgArrowUp;
	private Image imgArrowDown;
	
	private Sprite spriteClearRow;
	private Sprite spriteExplosion;
	private Sprite spriteBombPiledriver;
	private Sprite spriteBombFatboy;
	private Sprite spriteBombLeveller;

	private Animation animClearRow;
	private Animation animExplosion;
	private Animation animBombPiledriver;
	private Animation animBombFatboy;
	private Animation animBombLeveller;

	private int scrMidX;
	private int leftColX;
	private int rightColX;
	private int tileSize;
	private int stackPosX;
	private int previewPosY;
	private int colItemW;
	private final int border = 50;
	private final int statsBoxH = 40;
	private int metricsWBtnPause;
	private int metricsWBtnQuit;
	private int stackMidY;

	public ScreenGame(DisplayManager scrMgr, FontManager fntMgr) {
		this.scrMgr = scrMgr;
		this.fntMgr = fntMgr;

		loadImages();

		scrMidX = scrMgr.getWidth() / 2;
		tileSize = ((scrMgr.getHeight() - (border * 2)) / Constants.STACKHEIGHT);
		stackPosX = (scrMidX - (tileSize * (Constants.STACKWIDTH / 2)));
		leftColX = (stackPosX - (tileSize * 8));
		rightColX = (scrMidX + (tileSize * (Constants.STACKWIDTH / 2)) + (tileSize * 2));
		colItemW = (tileSize * 6);
		previewPosY = (border + (Constants.STACKHEIGHT * tileSize) - (tileSize * 6));
		metricsWBtnPause = fntMgr.getMetrics().stringWidth(Constants.BTNTXT_PAUSE_GAME);
		metricsWBtnQuit = fntMgr.getMetrics().stringWidth(Constants.BTNTXT_QUIT_GAME);
		stackMidY = border + ((tileSize * Constants.STACKHEIGHT) / 2); 

	}
	
	@Override
	public void loadImages() {
		// TODO Auto-generated method stub
		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "brickwall.jpg")).getImage();
		imgIntro 			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "intro.jpg")).getImage();
		imgTileDarkBlue		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileDarkBlue.jpg")).getImage();
		imgTileRed			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileRed.jpg")).getImage();
		imgTileYellow		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileYellow.jpg")).getImage();
		imgTileLightBlue	= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileLightBlue.jpg")).getImage();
		imgTileGreen		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileGreen.jpg")).getImage();
		imgTilePurple		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tilePurple.jpg")).getImage();
		imgTileOrange		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileOrange.jpg")).getImage();
		imgFatboyBlowing	= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy9.jpg")).getImage();

		imgArrowLeft		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "arrow-left.jpg")).getImage();
		imgArrowRight		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "arrow-right.jpg")).getImage();
		imgArrowUp			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "arrow-up.jpg")).getImage();
		imgArrowDown		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "arrow-down.jpg")).getImage();

		// Load images for clear row animation.
		animClearRow = new Animation();
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow1.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow2.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow3.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow4.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow5.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow6.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow5.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow4.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow3.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow2.jpg")).getImage(), 50);
		animClearRow.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileClearRow1.jpg")).getImage(), 50);

		animExplosion = new Animation();
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion1.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion2.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion3.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion4.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion5.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion4.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion3.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion2.jpg")).getImage(), 150);
		animExplosion.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tileExplosion1.jpg")).getImage(), 150);

		animBombPiledriver = new Animation();
		animBombPiledriver.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-piledriver1.jpg")).getImage(), 100);
		animBombPiledriver.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-piledriver2.jpg")).getImage(), 100);
		animBombPiledriver.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-piledriver3.jpg")).getImage(), 100);
		
		animBombFatboy = new Animation();
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy1.jpg")).getImage(), 2000);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy2.jpg")).getImage(), 500);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy1.jpg")).getImage(), 2000);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy3.jpg")).getImage(), 1000);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy1.jpg")).getImage(), 2000);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy4.jpg")).getImage(), 1500);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy5.jpg")).getImage(), 1500);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy1.jpg")).getImage(), 3000);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy6.jpg")).getImage(), 350);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy7.jpg")).getImage(), 350);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy6.jpg")).getImage(), 350);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy7.jpg")).getImage(), 350);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy6.jpg")).getImage(), 350);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy7.jpg")).getImage(), 350);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy1.jpg")).getImage(), 2000);
		animBombFatboy.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-fatboy8.jpg")).getImage(), 1500);
		
		animBombLeveller = new Animation();
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller1.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller2.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller3.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller2.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller1.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller4.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller5.jpg")).getImage(), 250);
		animBombLeveller.addScene(new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "bomb-leveller4.jpg")).getImage(), 250);
		
		// Create sprite for the clear row animation.
		spriteClearRow = new Sprite(animClearRow);
		spriteClearRow.setVelocityX(0);
		spriteClearRow.setVelocityY(0);
		
		// Create sprite for the bomb explosion animation.
		spriteExplosion = new Sprite(animExplosion);
		spriteExplosion.setVelocityX(0);
		spriteExplosion.setVelocityY(0);

		spriteBombPiledriver = new Sprite(animBombPiledriver);
		spriteBombPiledriver.setVelocityX(0);
		spriteBombPiledriver.setVelocityY(0);

		spriteBombFatboy = new Sprite(animBombFatboy);
		spriteBombPiledriver.setVelocityX(0);
		spriteBombPiledriver.setVelocityY(0);

		spriteBombLeveller = new Sprite(animBombLeveller);
		spriteBombLeveller.setVelocityX(0);
		spriteBombLeveller.setVelocityY(0);
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		g2d.drawImage(imgBackground, 0, 0, null);
		g2d.setColor(Color.black);
		g2d.fillRect(stackPosX, border, (tileSize * Constants.STACKWIDTH), (tileSize * Constants.STACKHEIGHT)); // Gamefield
//		g2d.drawImage(imgGamefieldBorder, stackPosX, 100, (tileSize * Constants.STACKWIDTH), (tileSize * Constants.STACKHEIGHT), null);
		g2d.fillRect(leftColX, previewPosY, colItemW, colItemW);
		g2d.drawImage(imgIntro, leftColX, border, colItemW, (int)(colItemW * 0.75f), null);

		g2d.setColor(Color.darkGray);
		g2d.fill3DRect(rightColX, previewPosY + (tileSize * 1), colItemW, tileSize, true);
		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.BTNTXT_HELP, rightColX + (colItemW / 2) - (metricsWBtnPause / 2), previewPosY + (tileSize * 1) + fntMgr.getFontSize() - 5);

		g2d.setColor(Color.darkGray);
		g2d.fill3DRect(rightColX, previewPosY + (tileSize * 3), colItemW, tileSize, true);
		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.BTNTXT_PAUSE_GAME, rightColX + (colItemW / 2) - (metricsWBtnPause / 2), previewPosY + (tileSize * 3) + fntMgr.getFontSize() - 5);

		g2d.setColor(Color.darkGray);
		g2d.fill3DRect(rightColX, previewPosY + (tileSize * 5), colItemW, tileSize, true);
		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.BTNTXT_QUIT_GAME, rightColX + (colItemW / 2) - (metricsWBtnQuit / 2), previewPosY + (tileSize * 5) + fntMgr.getFontSize() - 5);
	}

	// Draw Yes and No buttons for when prompted to quit game.
	public void drawPrompToQuit() {
		g2d.setColor(Color.DARK_GRAY);
		g2d.fillRect(stackPosX, stackMidY - (tileSize * 2), tileSize * Constants.STACKWIDTH, 200);
		g2d.setColor(Color.GRAY);
		g2d.fill3DRect(stackPosX + tileSize, stackMidY + 35, tileSize * 3, 35, true);
		g2d.fill3DRect(stackPosX + (tileSize * 6), stackMidY + 35, tileSize * 3, 35, true);
		g2d.setColor(Color.WHITE);
		g2d.drawString("Quit game?", scrMidX - (fntMgr.getMetrics().stringWidth("Quit game?") / 2), stackMidY);
//		g2d.drawString("Quit game?", stackPosX + (fntMgr.getMetrics().stringWidth("Yes") / 2), stackMidY + 60);
		g2d.drawString("Yes", stackPosX + (int)(tileSize * 2.5) - (fntMgr.getMetrics().stringWidth("Yes") / 2), stackMidY + 60);
		g2d.drawString("No", stackPosX + (int)(tileSize * 7.5) - (fntMgr.getMetrics().stringWidth("No") / 2), stackMidY + 60);
	}
	
	// Draw the stack.
	public void drawStack(Stack stack) {
		int x;
		int y;

		for (int i = 0; i < Constants.STACKWIDTH; i++) {
			for (int j = 0; j < Constants.STACKHEIGHT; j++) {
				if (stack.getStack()[i][j] != Constants.TILEEMPTY) {
					x = stackPosX + (i * tileSize);
					y = border + (j * tileSize);
					switch(stack.getStack()[i][j]) {
						case 0: g2d.drawImage(imgTileDarkBlue, x, y, tileSize, tileSize, null); break;
						case 1: g2d.drawImage(imgTileRed, x, y, tileSize, tileSize, null); break;
						case 2: g2d.drawImage(imgTileYellow, x, y, tileSize, tileSize, null); break;
						case 3: g2d.drawImage(imgTileLightBlue, x, y, tileSize, tileSize, null); break;
						case 4: g2d.drawImage(imgTileGreen, x, y, tileSize, tileSize, null); break;
						case 5: g2d.drawImage(imgTilePurple, x, y, tileSize, tileSize, null); break;
						case 6: g2d.drawImage(imgTileOrange, x, y, tileSize, tileSize, null); break;
						case 10: g2d.drawImage(spriteClearRow.getImage(), x, y, tileSize, tileSize, null); break;
					}
				}
			}
		}
	}

	public void drawExplosion(Stack stack) {
		int x;
		int y;

		for (int i = 0; i < Constants.STACKWIDTH; i++) {
			for (int j = 0; j < Constants.STACKHEIGHT; j++) {
				if (stack.getStack()[i][j] != Constants.TILEEMPTY) {
					x = stackPosX + (i * tileSize);
					y = border + (j * tileSize);
					switch(stack.getStack()[i][j]) {
					case 90: g2d.drawImage(spriteExplosion.getImage(), x, y, tileSize, tileSize, null); break;
					}
				}
			}
		}
	}
	
	// Draw the block.
	public void drawBlock(Block block) {
		int x;
		int y;

		for (int i = 0; i < 4; i++) {
			x = stackPosX + (block.getBlock()[i][0] * tileSize);
			y = border + (block.getBlock()[i][1] * tileSize);
			switch(block.getBlocknum()) {
				case 0: g2d.drawImage(imgTileDarkBlue, x, y, tileSize, tileSize, null); break;
				case 1: g2d.drawImage(imgTileRed, x, y, tileSize, tileSize, null); break;
				case 2: g2d.drawImage(imgTileYellow, x, y, tileSize, tileSize, null); break;
				case 3: g2d.drawImage(imgTileLightBlue, x, y, tileSize, tileSize, null); break;
				case 4: g2d.drawImage(imgTileGreen, x, y, tileSize, tileSize, null); break;
				case 5: g2d.drawImage(imgTilePurple, x, y, tileSize, tileSize, null); break;
				case 6: g2d.drawImage(imgTileOrange, x, y, tileSize, tileSize, null); break;
			}
		}
	}	
	
	
	public void drawBomb(int bombX, int bombY, String bombType, int timeTilExplosion) {
		final int x = stackPosX + (bombX * tileSize);
		final int y = border + (bombY * tileSize);

		if (bombType.contentEquals("Fatboy")) {
			if (timeTilExplosion <= 2) {
				g2d.drawImage(imgFatboyBlowing, x, y, tileSize, tileSize, null);
			} else {		
				g2d.drawImage(spriteBombFatboy.getImage(), x, y, tileSize, tileSize, null);
			}
		} else if (bombType.contentEquals("Leveller")) {
			g2d.drawImage(spriteBombLeveller.getImage(), x, y, tileSize, tileSize, null);
		} else if (bombType.contentEquals("Piledriver")) {
			g2d.drawImage(spriteBombPiledriver.getImage(), x, y, tileSize, tileSize, null);
		}
	}

	
	// Draw the block preview.
	public void drawBlockPreview(final int[][][] bs, final int nbnum) {
		int x;
		int y;

		g2d.setColor(Color.WHITE);
		g2d.drawString("Next Block", leftColX, previewPosY - fntMgr.getFontSize());
		
		// Draw container
		g2d.setColor(Color.black);
		g2d.fillRect(leftColX, previewPosY, colItemW, colItemW);

		// Draw each tile of nextblock.
		for (int i = 0; i < 4; i++) {
			x = leftColX + tileSize + ((bs[nbnum][i][0]) * tileSize);
			y = previewPosY + tileSize + (bs[nbnum][i][1]) * tileSize;

			switch(nbnum) {
				case 0: g2d.drawImage(imgTileDarkBlue, x  - (int)(3.5 * tileSize), y, tileSize, tileSize, null); break;
				case 1: g2d.drawImage(imgTileRed, x - (4 * tileSize), y + tileSize, tileSize, tileSize, null); break;
				case 2: g2d.drawImage(imgTileYellow, x - (4 * tileSize), y + (int)(0.5 * tileSize), tileSize, tileSize, null); break;
				case 3: g2d.drawImage(imgTileLightBlue, x - (4 * tileSize), y + (int)(0.5 * tileSize), tileSize, tileSize, null); break;
				case 4: g2d.drawImage(imgTileGreen, x - (4 * tileSize), y + (int)(0.5 * tileSize), tileSize, tileSize, null); break;
				case 5: g2d.drawImage(imgTilePurple, x - (4 * tileSize), y + (int)(0.5 * tileSize), tileSize, tileSize, null); break;
				case 6: g2d.drawImage(imgTileOrange, x - (int)(3.5 * tileSize), y + tileSize, tileSize, tileSize, null); break;
			}
		}
	}
	
	
	// Draw the player game stats.
	public void drawStats(final Stack stack, final Score score, final PropertiesManager propMgr, final long timeElapsed) {
		long timeMillis = timeElapsed;
		long time = timeMillis / 1000;
		String seconds = Integer.toString((int)(time % 60));
		String minutes = Integer.toString((int)((time % 3600) / 60));
		String hours = Integer.toString((int)(time / 3600));
		for (int i = 0; i < 2; i++) {
			if (seconds.length() < 2) {
				seconds = "0" + seconds;
			}
			if (minutes.length() < 2) {
				minutes = "0" + minutes;
			}
			if (hours.length() < 2) {
				hours = "0" + hours;
			}
		}
		String strTimeElapsed = hours + ":" + minutes + ":" + seconds;

		g2d.setColor(Color.BLACK);
		g2d.fillRect(rightColX, border + statsBoxH, colItemW, statsBoxH);  // Score
		g2d.fillRect(rightColX, border + (statsBoxH * 3), colItemW, statsBoxH);  // Level
		g2d.fillRect(rightColX, border + (statsBoxH * 5), colItemW, statsBoxH);  // Rows Cleared
		g2d.fillRect(rightColX, border + (statsBoxH * 7), colItemW, statsBoxH);  // Streak

		g2d.setColor(Color.WHITE);
		g2d.drawString("Score"			, rightColX, border + statsBoxH);
		g2d.drawString("Level"			, rightColX, border + (statsBoxH * 3));
		g2d.drawString("Rows Cleared"	, rightColX, border + (statsBoxH * 5));
		g2d.drawString("Streak"			, rightColX, border + (statsBoxH * 7));
		
		g2d.setColor(Color.WHITE);
		g2d.drawString(Integer.toString(score.getScore())			, rightColX + 10, border + (statsBoxH * 2) - 5);
		g2d.drawString(Integer.toString(score.getLevel())			, rightColX + 10, border + (statsBoxH * 4) - 5);
		g2d.drawString(Integer.toString(stack.getTotRowsCleared())	, rightColX + 10, border + (statsBoxH * 6) - 5);
		g2d.drawString(Integer.toString(score.getStreak())			, rightColX + 10, border + (statsBoxH * 8) - 5);

		if (propMgr.getProperty("displayTimeElapsed").contentEquals("true")) {
			g2d.setColor(Color.BLACK);
			g2d.fillRect(rightColX, border + (statsBoxH * 9), colItemW, statsBoxH);  // Timer
			g2d.setColor(Color.WHITE);
			g2d.drawString("Time Elapsed"	, rightColX, border + (statsBoxH * 9));
			g2d.drawString(strTimeElapsed	, rightColX + 10, border + (statsBoxH * 10) - 5);
		}

	
	}

	public void drawDebugMsg(String strDbgMsg) {
		g2d.drawString(strDbgMsg, 0, fntMgr.getFontSize());
	}
	
	// Draw an alert message in the middle of the gamefield (e.g. 'Get ready', 'Game paused', 'Game over').
	public void drawAlertMsg(String strAlrtMsg) {
		if (!strAlrtMsg.contentEquals("")) {
			g2d.setColor(Color.DARK_GRAY);
			g2d.fillRect(stackPosX, border + ((tileSize * Constants.STACKHEIGHT) / 2) - (tileSize * 2), tileSize * Constants.STACKWIDTH, (tileSize * 4));
	
			g2d.setColor(Color.WHITE);
			g2d.drawString(strAlrtMsg, scrMidX - (fntMgr.getMetrics().stringWidth(strAlrtMsg) / 2), border + ((tileSize * Constants.STACKHEIGHT) / 2) - tileSize);

			g2d.drawString("Press any key to continue", scrMidX - (fntMgr.getMetrics().stringWidth("Press any key to continue") / 2), border + ((tileSize * Constants.STACKHEIGHT) / 2) + (tileSize * 1));
		}
	}
	
	public void drawQuitGameMsg() {
		g2d.setColor(Color.DARK_GRAY);
		g2d.fillRect(stackPosX, stackMidY - (tileSize), tileSize * Constants.STACKWIDTH, (tileSize * 3));

		g2d.setColor(Color.WHITE);
		g2d.drawString("Quit game?", scrMidX - (fntMgr.getMetrics().stringWidth("Quit game?") / 2), border + ((tileSize * Constants.STACKHEIGHT) / 2) - tileSize);
		g2d.drawString("Left-mouse button or N to keep playing.", scrMidX - (fntMgr.getMetrics().stringWidth("Left-mouse button or N to keep playing.") / 2), (border * 2) + 50 + ((tileSize * Constants.STACKHEIGHT) / 2));
		g2d.drawString("Right-mouse button or Y to quit.", scrMidX - (fntMgr.getMetrics().stringWidth("Right-mouse button or Y to quit.") / 2), 200 + ((tileSize * Constants.STACKHEIGHT) / 2));
	}
	
	public void drawHelp() {
		g2d.setColor(Color.DARK_GRAY);
		g2d.fillRect(stackPosX, stackMidY - (tileSize * 5), tileSize * Constants.STACKWIDTH, (tileSize * 10));

		g2d.drawImage(imgArrowLeft	, scrMidX - (int)(tileSize * 2.5), stackMidY - (int)(tileSize * 2.7), tileSize - 5, tileSize - 5, null);
		g2d.drawImage(imgArrowRight	, scrMidX - (int)(tileSize * 2.5), stackMidY - (int)(tileSize * 1.7), tileSize - 5, tileSize - 5, null);
		g2d.drawImage(imgArrowDown	, scrMidX - (int)(tileSize * 2.5), stackMidY - (int)(tileSize * 0.7), tileSize - 5, tileSize - 5, null);
		g2d.drawImage(imgArrowDown	, scrMidX - (int)(tileSize * 2.5), stackMidY + (int)(tileSize * 0.3), tileSize - 5, tileSize - 5, null);
		g2d.drawImage(imgArrowUp	, scrMidX - (int)(tileSize * 2.5), stackMidY + (int)(tileSize * 1.3), tileSize - 5, tileSize - 5, null);

		g2d.setColor(Color.WHITE);
		g2d.drawString("Help", scrMidX - (fntMgr.getMetrics().stringWidth("Help") / 2), stackMidY - (tileSize * 4));

		g2d.drawString("Move block left"	, scrMidX - tileSize		, stackMidY - (tileSize * 2));
		g2d.drawString("Move block right"	, scrMidX - tileSize		, stackMidY - (tileSize * 1));
		g2d.drawString("Move block down"	, scrMidX - tileSize		, stackMidY);
		g2d.drawString("Hold"				, scrMidX - (tileSize * 4)	, stackMidY);
		g2d.drawString("Fast drop"			, scrMidX - tileSize		, stackMidY + (tileSize * 1));
		g2d.drawString("Rotate block"		, scrMidX - tileSize		, stackMidY + (tileSize * 2));

		g2d.drawString("Press any key to continue", scrMidX - (fntMgr.getMetrics().stringWidth("Press any key to continue") / 2), stackMidY + (int)(tileSize * 3.5));
	}
	
	public Sprite getSpriteClearRow() {
		return spriteClearRow;
	}
	
	public Sprite getSpriteExplosion() {
		return spriteExplosion;
	}
	
	public void start() {
		g2d = scrMgr.getGraphics();
	}

	public void finalise() {
		g2d.dispose();
		scrMgr.update();
	}
	
	@Override
	public int checkMousePos(int mouseX, int mouseY) {
		// TODO Auto-generated method stub
		int itemInBounds = 0;

		if (((mouseX >= rightColX) && (mouseX < rightColX + colItemW)) &&
		    ((mouseY >= previewPosY + (tileSize * 1)) && (mouseY < previewPosY + (tileSize * 2)))) {
			itemInBounds = 1;
		} else if (((mouseX >= rightColX) && (mouseX < rightColX + colItemW)) &&
		    ((mouseY >= previewPosY + (tileSize * 3)) && (mouseY < previewPosY + (tileSize * 4)))) {
			itemInBounds = 2;
		} else if (((mouseX >= rightColX) && (mouseX < rightColX + colItemW)) &&
		   ((mouseY >= previewPosY + (tileSize * 5)) && (mouseY < previewPosY + (tileSize * 6)))) {
			itemInBounds = 3;
		} else if (((mouseX >= stackPosX + tileSize) && (mouseX <= stackPosX + (tileSize * 4))) &&
			(mouseY >= (border + (tileSize * Constants.STACKHEIGHT) / 2) + 40) && (mouseY <= (stackMidY + 75))) {
			itemInBounds = 10;
		} else if (((mouseX >= stackPosX + (tileSize * 6)) && (mouseX <= stackPosX + (tileSize * 9))) &&
			(mouseY >= (border + (tileSize * Constants.STACKHEIGHT / 2) + 40) && (mouseY <= (stackMidY + 75)))) {
			itemInBounds = 11;
		}

		return itemInBounds;
	}

}
