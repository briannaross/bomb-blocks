package blocks;

import java.util.Random;

public class Block {

	// [x][][] is the block number, [][y][] is the rotation number, [][][z][] is the square number
	// [][][][0] is the x change, [][][][1] is the y change
	private final static int ROTATIONS[][][][] =
		 {{{{-2, 2},{-1, 1},{ 0, 0},{ 1,-1}},
		   {{ 2,-2},{ 1,-1},{ 0, 0},{-1, 1}},
		   {{-2, 2},{-1, 1},{ 0, 0},{ 1,-1}},
		   {{ 2,-2},{ 1,-1},{ 0, 0},{-1, 1}}},
		  {{{ 0, 0},{ 0, 0},{ 0, 0},{ 0, 0}},
		   {{ 0, 0},{ 0, 0},{ 0, 0},{ 0, 0}},
		   {{ 0, 0},{ 0, 0},{ 0, 0},{ 0, 0}},
		   {{ 0, 0},{ 0, 0},{ 0, 0},{ 0, 0}}},
		  {{{ 2, 0},{ 1, 1},{ 0, 0},{-1, 1}},
		   {{-2, 0},{-1,-1},{ 0, 0},{ 1,-1}},
		   {{ 2, 0},{ 1, 1},{ 0, 0},{-1, 1}},
		   {{-2, 0},{-1,-1},{ 0, 0},{ 1,-1}}},
		  {{{ 0,-2},{-1,-1},{ 0, 0},{-1, 1}},
		   {{ 0, 2},{ 1, 1},{ 0, 0},{ 1,-1}},
		   {{ 0,-2},{-1,-1},{ 0, 0},{-1, 1}},
		   {{ 0, 2},{ 1, 1},{ 0, 0},{ 1,-1}}},
		  {{{ 0,-2},{ 1,-1},{ 0, 0},{-1, 1}},
		   {{-2, 0},{-1,-1},{ 0, 0},{ 1, 1}},
		   {{ 0, 2},{-1, 1},{ 0, 0},{ 1,-1}},
		   {{ 2, 0},{ 1, 1},{ 0, 0},{-1,-1}}},
		  {{{ 2, 0},{ 1,-1},{ 0, 0},{-1, 1}},
		   {{ 0,-2},{-1,-1},{ 0, 0},{ 1, 1}},
		   {{-2, 0},{-1, 1},{ 0, 0},{ 1,-1}},
		   {{ 0, 2},{ 1, 1},{ 0, 0},{-1,-1}}},
		  {{{-1, 1},{ 1, 1},{ 0, 0},{-1,-1}},
		   {{ 1, 1},{ 1,-1},{ 0, 0},{-1, 1}},
		   {{ 1,-1},{-1,-1},{ 0, 0},{ 1, 1}},
		   {{-1,-1},{-1, 1},{ 0, 0},{ 1,-1}}}};
	private transient final int[][][] allBlocks =
		   {{{5,0},{5,1},{5,2},{5,3}},
			{{5,1},{6,1},{5,0},{6,0}},
			{{5,2},{5,1},{6,1},{6,0}},
			{{6,2},{6,1},{5,1},{5,0}},
			{{6,2},{5,2},{5,1},{5,0}},
			{{5,2},{6,2},{6,1},{6,0}},
			{{5,0},{4,1},{5,1},{6,1}}};
	private transient int theBlock[][] = new int[Constants.TILESINBLOCK][2];   // Current block - verified, legal block.
	private transient int propBlock[][] = new int[Constants.TILESINBLOCK][2];  // Proposed block - block with an unverified, potentially illegal transform applied. 
	private transient final int blockNum;
	private transient int rotSet = 0;
	private	transient final Random generator = new Random();
	private transient boolean boolDfrStckBlck = false;
	private transient boolean boolStckBlckNw = false;
	private transient boolean boolBirthMe = false;
	
	
	public Block() {
		blockNum = generator.nextInt(Constants.MAXTILES);

		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
			for (int j = 0; j < 2; j++) {	
				theBlock[i][j] = allBlocks[blockNum][i][j];
			}
		}

		
	}
	
	public void rotateBlock() {
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
				propBlock[i][0] = theBlock[i][0] + ROTATIONS[blockNum][rotSet][i][0];
				propBlock[i][1] = theBlock[i][1] + ROTATIONS[blockNum][rotSet][i][1];
		}
	}


	public void moveBlockLeft() {
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	 {
			propBlock[i][0] = theBlock[i][0] - 1;
			propBlock[i][1] = theBlock[i][1];
		}
	}


	public void moveBlockRight() {
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	 {
			propBlock[i][0] = theBlock[i][0] + 1;
			propBlock[i][1] = theBlock[i][1];
		}
	}


	public void moveBlockDown() {
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
			propBlock[i][0] = theBlock[i][0];
			propBlock[i][1] = theBlock[i][1] + 1;
		}
	}

	public void instantBlockDown(int dropY) {
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
			propBlock[i][0] = theBlock[i][0];
			propBlock[i][1] = theBlock[i][1] + dropY;
		}
	}

	public void incrementRotationset() {
		rotSet++;
		rotSet = (rotSet % 4);
	}

	public int[][] getBlock() {
		return theBlock.clone();
	}

	public int[][] getPropblock() {
		return propBlock.clone();
	}

	public void confirmPropblock() {
		theBlock = propBlock;
		propBlock = new int[Constants.TILESINBLOCK][2];
	}

	public int[][][] getAllBlocks() {
		return allBlocks.clone();
	}

	public int getBlocknum() {
		return blockNum;
	}

	public boolean checkDeferStackBlock() {
		return boolDfrStckBlck;
	}

	public void setDeferStackBlock(final boolean boolDfrStckBlck) {
		this.boolDfrStckBlck = boolDfrStckBlck;
	}

	public boolean checkStackBlockNow() {
		return boolStckBlckNw;
	}

	public void setStackBlockNow(final boolean boolStackBlockNow) {
		this.boolStckBlckNw = boolStackBlockNow;
	}

	public boolean birthMeYet() {
		return boolBirthMe;
	}

	public void prepareForBirth() {
		boolBirthMe = true;
	}
}

