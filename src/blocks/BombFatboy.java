package blocks;

import java.util.Random;

public class BombFatboy extends AbstractBomb {

	public BombFatboy(final int startX, final int interval) {
		// TODO Auto-generated constructor stub
		super (startX, interval);

		strBombType = "Fatboy";
	}

	@Override
	public void setTilesToExplode() {
		final Random generator = new Random();
		// TODO Auto-generated method stub
		explodeY = generator.nextInt(Constants.STACKHEIGHT - 1);
		
		minExplodeX = bombX - 1;
		maxExplodeX = bombX + 1;
		minExplodeY = explodeY - 1;
		maxExplodeY = explodeY + 1;

		if (minExplodeX < 0) {
			minExplodeX = 0;
		} else if (minExplodeX >= Constants.STACKWIDTH) {
			minExplodeX = Constants.STACKWIDTH - 1;
		}
		
		if (maxExplodeX < 0) {
			maxExplodeX = 0;
		} else if (maxExplodeX >= Constants.STACKWIDTH) {
			maxExplodeX = Constants.STACKWIDTH - 1;
		}
		
		if (minExplodeY < 0) {
			minExplodeY = 0;
		} else if (minExplodeY >= Constants.STACKHEIGHT) {
			minExplodeY = Constants.STACKHEIGHT - 1;
		}
		
		if (maxExplodeY < 0) {
			maxExplodeY = 0;
		} else if (maxExplodeY >= Constants.STACKHEIGHT) {
			maxExplodeY = Constants.STACKHEIGHT - 1;
		}
	}

	@Override
	public boolean explode(final boolean resting) {
		// TODO Auto-generated method stub
		boolean explodeNow = false;

		if (bombY == explodeY) {
			explodeNow = true;
		}

		return explodeNow;
	}

}
