package blocks;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
// import java.lang.reflect.InvocationTargetException;
import javax.swing.JFrame;

public class DisplayManager {

	private transient final GraphicsDevice gDevice;
//	private JFrame f = new JFrame();

	private static final DisplayMode DISPLAY_MODES[] = {
		new DisplayMode(1920, 1200, 32, 0),
		new DisplayMode(1920, 1200, 24, 0),
		new DisplayMode(1920, 1200, 16, 0),
		new DisplayMode(1920, 1080, 32, 0),
		new DisplayMode(1920, 1080, 24, 0),
		new DisplayMode(1920, 1080, 16, 0),
		new DisplayMode(1440, 900, 32, 0),
		new DisplayMode(1440, 900, 24, 0),
		new DisplayMode(1440, 900, 16, 0),
		new DisplayMode(1280, 1024, 32, 0),
		new DisplayMode(1280, 1024, 24, 0),
		new DisplayMode(1280, 1024, 16, 0),
		new DisplayMode(1280, 800, 32, 0),
		new DisplayMode(1280, 800, 24, 0),
		new DisplayMode(1280, 800, 16, 0),
		new DisplayMode(1366, 768, 32, 0),
		new DisplayMode(1366, 768, 24, 0),
		new DisplayMode(1366, 768, 16, 0),
		new DisplayMode(1024, 768, 32, 0),
		new DisplayMode(1024, 768, 24, 0),
		new DisplayMode(1024, 768, 16, 0),
	};
	

	//give vc access to monitor screen
	public DisplayManager() {
		final GraphicsEnvironment grphcsEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		gDevice = grphcsEnv.getDefaultScreenDevice();
	}
	
	//get all compatible display modes from the video card
	public DisplayMode[] getCompatibleDisplayModes() {
		return gDevice.getDisplayModes();
	}


	//compares DV passed in to vc DM and see if they match
	public DisplayMode findFirstCompatibleMode() {
		final DisplayMode goodModes[] = gDevice.getDisplayModes();
		for (int x = 0; x < DISPLAY_MODES.length; x++) {
			for (int y = 0; y < goodModes.length; y++) {
				if (displayModesMatch(DISPLAY_MODES[x], goodModes[y])) {
					return DISPLAY_MODES[x];
				}
			}
		}
		return null;
	}
	
	//get current DM
	public DisplayMode getCurrentDisplayMode() {
		return gDevice.getDisplayMode();
	}
	
	//checks if two modes match each other
	public boolean displayModesMatch(final DisplayMode dsplyMd1, final DisplayMode dsplyMd2) {
		if ((dsplyMd1.getWidth() != dsplyMd2.getWidth()) || (dsplyMd2.getHeight() != dsplyMd2.getHeight())) {
			return false;
		}
		if ((dsplyMd1.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI) && (dsplyMd2.getBitDepth() != DisplayMode.BIT_DEPTH_MULTI) && (dsplyMd1.getBitDepth() != dsplyMd2.getBitDepth())) {
			return false;
		}
		if ((dsplyMd1.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN) && (dsplyMd2.getRefreshRate() != DisplayMode.REFRESH_RATE_UNKNOWN) && (dsplyMd1.getRefreshRate() != dsplyMd2.getRefreshRate())) {
			return false;
		}
		return true;
	}
	
	//make frame full screen
	public void setFullScreen(final DisplayMode dsplyMd) {
		final JFrame frame = new JFrame();

//		frame.setUndecorated(true);
//		frame.setIgnoreRepaint(true);
//		frame.setResizable(false);

		frame.setUndecorated(false);
		frame.setIgnoreRepaint(true);
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		gDevice.setFullScreenWindow(frame);

		if ((dsplyMd != null) && (gDevice.isDisplayChangeSupported())) {
			try {
				gDevice.setDisplayMode(dsplyMd);
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
		frame.createBufferStrategy(2);
	}
	
	//we will set Graphics object equal to 'this'
	public Graphics2D getGraphics() {
		final Window window = gDevice.getFullScreenWindow();
		if (window == null) {
			return null;
		} else {
			final BufferStrategy buffStrtgy = window.getBufferStrategy();
			return (Graphics2D)buffStrtgy.getDrawGraphics();
		}
	}
	
	//updates display
	public void update() {
		final Window window = gDevice.getFullScreenWindow();
		if (window != null) {
			final BufferStrategy buffStrtgy = window.getBufferStrategy();
			if (!buffStrtgy.contentsLost()) {
				buffStrtgy.show();
			}
		}
	}
	
	//returns full screen window
	public Window getFullScreenWindow() {
		return gDevice.getFullScreenWindow();
	}

	//get width of window
	public int getWidth() {
		final Window window = gDevice.getFullScreenWindow();
		if (window == null) {
			return 0;
		} else {
			return window.getWidth();
		}
	}

	//get height of window
	public int getHeight() {
		final Window window = gDevice.getFullScreenWindow();
		if (window == null) {
			return 0;
		} else {
			return window.getHeight();
		}
	}

	//get out of full screen mode
	public void restoreScreen() {
		final Window window = gDevice.getFullScreenWindow();
		if (window != null) {
			window.dispose();
		}
		gDevice.setFullScreenWindow(null);
	}

	//create image compatible with monitor
	public BufferedImage createCompatibleImage(final int width, final int height, final int transparency) {
		final Window win = gDevice.getFullScreenWindow();
		if (win != null) {
			final GraphicsConfiguration grphcsCnfg = win.getGraphicsConfiguration();
			return grphcsCnfg.createCompatibleImage(width, height, transparency);
		}
		return null;
	}
}
