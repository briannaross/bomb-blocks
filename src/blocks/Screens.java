package blocks;

public final class Screens {
    private Screens() {
    	throw new AssertionError();
    }  // Prevents instantiation

	public static ScreenAppStart screenAppStart;
	public static ScreenMainMenu screenMainMenu;
	public static ScreenConfig screenConfig;
	public static ScreenLadder screenLadder;
	public static ScreenGame screenGame;
	public static ScreenAppEnd screenAppEnd;
}
