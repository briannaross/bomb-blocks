package blocks;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

// import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

public class Animation {

	private transient final List<OneScene> scenes;
	
	private transient int sceneIndex;
	private transient int totCycles;
	
	private transient long movieTime;
	private transient long totalTime;

	
	//Constructor
	public Animation() {
		scenes = new ArrayList<OneScene>();
		totalTime = 0;
		totCycles = 0;
		movieTime = 0;
		sceneIndex = 0;
	}
	
	
	//add scene to ArrayList and set time for each scene
	public void addScene(final Image img, final long totTime) {
		synchronized(this) {
			totalTime += totTime;
			scenes.add(new OneScene(img, totalTime));
		}
	}

	
	public void start() {
		synchronized(this) {
			movieTime = 0;
			sceneIndex = 0;
		}
	}
	
	
	// Change the scene to the next in the arraylist.
	public void update(final long timePassed) {
		synchronized(this) {
			if (scenes.size() > 1) {
				movieTime += timePassed;
	
				if (movieTime >= totalTime) {
					movieTime = 0;
					sceneIndex = 0;
					totCycles++;
				}
				
				while (movieTime > getScene(sceneIndex).endTime) {
					sceneIndex++;
				}
			}
		}
	}
	
	
	// Get the current scene in the animation.
	public Image getImage() {
		synchronized(this) {
			Image img = null;
			
			if (!scenes.isEmpty()) {
				img = getScene(sceneIndex).pic;
			}
			
			return img;
		}
	}
	
	
	public int getNumCycles() {
		return totCycles;
	}
	
	
	// Reset the number of cycles the animation has gone through.
	public void resetCycles() {
		totCycles = 0;
	}
	
	
	// Get the specified scene
	private OneScene getScene(final int sceneX) {
		return (OneScene)scenes.get(sceneX);
	}
	
	
	// Private inner class
	private class OneScene{
		private final transient Image pic;
		private final transient long endTime;
		
		public OneScene(final Image pic, final long endTime) {
			this.pic = pic;
			this.endTime = endTime;
		}
	}
	
}
