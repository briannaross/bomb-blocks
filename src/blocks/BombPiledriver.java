package blocks;

//The Leveller bomb will detonate on touching a tile on the stack and will set the blocks to explode
public class BombPiledriver extends AbstractBomb {

	public BombPiledriver(final int startX, final int interval) {
		// TODO Auto-generated constructor stub
		super(startX, interval);

		strBombType = "Piledriver";
}

	@Override
	public void setTilesToExplode() {
		// TODO Auto-generated method stub
		minExplodeX = bombX;
		maxExplodeX = bombX;
		minExplodeY = 0;
		maxExplodeY = Constants.STACKHEIGHT - 1;
	}

	@Override
	public boolean explode(final boolean resting) {
		// TODO Auto-generated method stub
		boolean explodeNow = false;

		if (resting) {
			explodeY = bombY;
			explodeNow = true;
		}

		return explodeNow;
	}

}
