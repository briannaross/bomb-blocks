package blocks;

import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.io.InputStream;

public class FontManager {

	private Graphics2D g2d;
	private Font font;

	private int fontSize;

	FontManager(final DisplayManager scrMgr, final String fontName) {
		g2d = scrMgr.getGraphics();

		try {
			loadFont(fontName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fontSize = (int)(scrMgr.getHeight() / 32);
	}
	
	private void loadFont(String fontName) throws IOException, FontFormatException {
		InputStream is = this.getClass().getResourceAsStream(Constants.PATH_FONTS + fontName);
		Font fontRaw = Font.createFont(Font.TRUETYPE_FONT, is);
		font = fontRaw.deriveFont(24.0f);
		GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
		is.close();
	}
	
	public Font getFont() {
		return font;
	}
	
	public FontMetrics getMetrics() {
		return g2d.getFontMetrics(font);
	}
	
	public int getFontSize() {
		return fontSize;
	}
}
