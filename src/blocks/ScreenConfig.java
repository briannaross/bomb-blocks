package blocks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;

public class ScreenConfig implements IScreen {

	private static final String CFGOPT_ACTVT_BMBS = "F2 - Activate Bombs";
	private static final String CFGOPT_DSPLY_NXT_BLCK = "F3 - Display Next Block";
	private static final String CFGOPT_DSPLY_TM_ELPSD = "F4 - Display Time Elapsed";
	private static final String CFGOPT_INSTNT_DRP = "F5 - Instant Block Drop";
	private static final String CFGOPT_BMB_SPD = "F6 - Bomb Frequency";
	private static final String CFGOPT_STRT_LVL = "F7 - Start at level";
	private static final String CFGOPT_SFX_VOL = "F8 - Sound Effects Volume";
	private static final String CFGOPT_MSC_VOL = "F9 - Music Volume";

	//	private Image imgBackground;
	private Image imgIntro;
	private Image imgTick;
	private Image imgCross;
	private Image imgLeftArrow;
	private Image imgRightArrow;
	
	private PropertiesManager propMgr;

	private Graphics2D g2d;
	private DisplayManager scrMgr;
	private FontManager fntMgr;

	private int scrMidX;
	private int border;
	private int introImgH;
	private int menuLineH;
	private int tpMenuY;
	private int tpMenuYBxOffst;
	private int tpMenuYChckOffst;
	private int optBxSdLen;
	private int optChckSdLen;
	private int ltMenuX;
	private int ltOptX;
	private int ltOptXBxOffst;
	private int optArrwH;
	private int optArrwW;
	private int rtMenuX;
//	private int rtOptX;
	private int rtOptDcrsBtnX;
	private int rtOptIncrsBtnX;
	private int rtOptValMidX;
	private int rtOptArrwYOffst;
	private int metricsBtMMstr;

	
	ScreenConfig(DisplayManager scrMgr, FontManager fntMgr, PropertiesManager propMgr) {
		this.scrMgr = scrMgr;
		this.fntMgr = fntMgr;
		
		scrMidX = scrMgr.getWidth() / 2;
		border = 50;
		introImgH = 240;
		menuLineH = 50;
		
		// Offsets for the top of the menu.
		tpMenuY = (border * 2) + introImgH;
		tpMenuYBxOffst = tpMenuY - 20; 
		tpMenuYChckOffst = tpMenuY - 18; 

		// Offsets for the left side of the menu.
		optBxSdLen = 31;
		optChckSdLen = 27;
		ltMenuX = scrMidX - 510;
		ltOptX = scrMidX - 110 - optBxSdLen;
		ltOptXBxOffst = ltOptX + 2;

		// Offsets for right side of the menu. 
		optArrwH = 40;
		optArrwW = 20;
		rtMenuX = scrMidX + 10;
//		rtOptX = scrMidX + (fntMgr.getFontSize() * 10);
		rtOptDcrsBtnX = scrMidX + 280;
		rtOptIncrsBtnX = scrMidX + 490;
		rtOptValMidX = scrMidX + 395;
		rtOptArrwYOffst = tpMenuY - 25;

		// Offsets for the bottom of the menu.
		metricsBtMMstr = fntMgr.getMetrics().stringWidth(Constants.MENUTXT_BACKTOMM);

		loadImages();
	}
	
	
	@Override
	public void loadImages() {
		// TODO Auto-generated method stub
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "background.jpg")).getImage();
//		imgBackground 	= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "brickwall.jpg")).getImage();
		imgIntro 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "intro.jpg")).getImage();
		imgTick			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "tick.jpg")).getImage();
		imgCross		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "cross.jpg")).getImage();
		imgLeftArrow	= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "leftarrow.jpg")).getImage();
		imgRightArrow	= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "rightarrow.jpg")).getImage();

	}

	
	@Override
	public void draw() {
		// TODO Auto-generated method stub
		String strBombFrequency = getBombFrequency();

		g2d = scrMgr.getGraphics();
		
		// CENTRE TOP
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, scrMgr.getWidth(), scrMgr.getHeight());	 	// Gamefield
		g2d.drawImage(imgIntro, scrMidX - 160, border, 320, introImgH, null);
		g2d.setColor(Color.WHITE);
		g2d.setStroke(new BasicStroke(3));
		g2d.drawString("Config", scrMidX - ((fntMgr.getMetrics().stringWidth("Config") / 2)), tpMenuY);

		// COLUMN 1
		// Row 1
		g2d.drawString(CFGOPT_ACTVT_BMBS, ltMenuX, tpMenuY + menuLineH);
		g2d.drawRect(ltOptX, menuLineH + tpMenuYBxOffst, optBxSdLen, optBxSdLen);
		if (propMgr.getProperty("bombsActivated").contentEquals("true")) {
			g2d.drawImage(imgTick	, ltOptXBxOffst, tpMenuYChckOffst + menuLineH, optChckSdLen, optChckSdLen, null);
		} else {
			g2d.drawImage(imgCross	, ltOptXBxOffst + 2, tpMenuYChckOffst + menuLineH, optChckSdLen, optChckSdLen, null);
		}

		// Row 2
		g2d.drawString(CFGOPT_DSPLY_NXT_BLCK, ltMenuX, tpMenuY + (menuLineH * 2));
		g2d.drawRect(ltOptX, (menuLineH * 2) + tpMenuYBxOffst, optBxSdLen, optBxSdLen);
		if (propMgr.getProperty("displayNextBlock").contentEquals("true")) {
			g2d.drawImage(imgTick	, ltOptXBxOffst, tpMenuYChckOffst + (menuLineH * 2), optChckSdLen, optChckSdLen, null);
		} else {
			g2d.drawImage(imgCross	, ltOptXBxOffst, tpMenuYChckOffst + (menuLineH * 2), optChckSdLen, optChckSdLen, null);
		}

		// Row 3
		g2d.drawString(CFGOPT_DSPLY_TM_ELPSD, ltMenuX, tpMenuY + (menuLineH * 3));
		g2d.drawRect(ltOptX, (menuLineH * 3) + tpMenuYBxOffst, optBxSdLen, optBxSdLen);
		if (propMgr.getProperty("displayTimeElapsed").contentEquals("true")) {
			g2d.drawImage(imgTick	, ltOptXBxOffst, tpMenuYChckOffst + (menuLineH * 3), optChckSdLen, optChckSdLen, null);
		} else {
			g2d.drawImage(imgCross	, ltOptXBxOffst, tpMenuYChckOffst + (menuLineH * 3), optChckSdLen, optChckSdLen, null);
		}

		// Row 4
		g2d.drawString(CFGOPT_INSTNT_DRP, ltMenuX, tpMenuY + (menuLineH * 4));
		g2d.drawRect(ltOptX, (menuLineH * 4) + tpMenuYBxOffst, optBxSdLen, optBxSdLen);
		if (propMgr.getProperty("instantDrop").contentEquals("true")) {
			g2d.drawImage(imgTick	, ltOptXBxOffst, tpMenuYChckOffst + (menuLineH * 4), optChckSdLen, optChckSdLen, null);
		} else {
			g2d.drawImage(imgCross	, ltOptXBxOffst, tpMenuYChckOffst + (menuLineH * 4), optChckSdLen, optChckSdLen, null);
		}

		// COLUMN 2
		// Row 1
		g2d.drawString(CFGOPT_BMB_SPD	, rtMenuX, tpMenuY + menuLineH);
		g2d.drawImage(imgLeftArrow	, rtOptDcrsBtnX, menuLineH + rtOptArrwYOffst, optArrwW, optArrwH, null);
		g2d.drawString(strBombFrequency,  rtOptValMidX - (fntMgr.getMetrics().stringWidth(strBombFrequency) / 2), tpMenuY + menuLineH);
		g2d.drawImage(imgRightArrow	, rtOptIncrsBtnX, menuLineH + rtOptArrwYOffst, optArrwW, optArrwH, null);
		
		// Row 2
		g2d.drawString(CFGOPT_STRT_LVL, rtMenuX, tpMenuY + (menuLineH * 2));
		g2d.drawImage(imgLeftArrow	, rtOptDcrsBtnX, (menuLineH * 2) + rtOptArrwYOffst, optArrwW, optArrwH, null);
		g2d.drawString(propMgr.getProperty("startingLevel"), rtOptValMidX - (fntMgr.getMetrics().stringWidth(propMgr.getProperty("startingLevel")) / 2), tpMenuY + (menuLineH * 2));
		g2d.drawImage(imgRightArrow	, rtOptIncrsBtnX, (menuLineH * 2) + rtOptArrwYOffst, optArrwW, optArrwH, null);

		// Row 3
		g2d.drawString(CFGOPT_SFX_VOL, rtMenuX, tpMenuY + (menuLineH * 3));
		g2d.drawImage(imgLeftArrow	, rtOptDcrsBtnX, (menuLineH * 3) + rtOptArrwYOffst, optArrwW, optArrwH, null);
		g2d.drawString(propMgr.getProperty("soundEffectsVolume"), rtOptValMidX - (fntMgr.getMetrics().stringWidth(propMgr.getProperty("soundEffectsVolume")) / 2), tpMenuY + (menuLineH * 3));
		g2d.drawImage(imgRightArrow	, rtOptIncrsBtnX, (menuLineH * 3) + rtOptArrwYOffst, optArrwW, optArrwH, null);

		// Row 4
		g2d.drawString(CFGOPT_MSC_VOL, rtMenuX, tpMenuY + (menuLineH * 4));
		g2d.drawImage(imgLeftArrow	, rtOptDcrsBtnX, (menuLineH * 4)  + rtOptArrwYOffst, optArrwW, optArrwH, null);
		g2d.drawString(propMgr.getProperty("musicVolume"), rtOptValMidX - (fntMgr.getMetrics().stringWidth(propMgr.getProperty("musicVolume")) / 2), tpMenuY + (menuLineH * 4));
		g2d.drawImage(imgRightArrow	, rtOptIncrsBtnX, (menuLineH * 4) + rtOptArrwYOffst, optArrwW, optArrwH, null);

		// CENTRE BOTTOM
		g2d.setColor(Color.darkGray);
		g2d.fill3DRect(scrMidX - (metricsBtMMstr / 2) - 10,  tpMenuY + (menuLineH * 6), metricsBtMMstr + 20, 40, true);
		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.MENUTXT_BACKTOMM, scrMidX - (metricsBtMMstr / 2), tpMenuY + (menuLineH * 6) + 30);

		g2d.dispose();
		scrMgr.update();
	}


	@Override
	public int checkMousePos(int x, int y) {
		// TODO Auto-generated method stub
		int itemInBounds = 0;
		
		// LEFT COLUMN
		if ((x >= ltOptX) && (x <= (ltOptX + optBxSdLen)) &&
			(y >= (menuLineH + tpMenuYBxOffst)) && (y <= (menuLineH + tpMenuYBxOffst + optBxSdLen))) {
			itemInBounds = 1;
		} else if ((x >= ltOptX) && (x <= (ltOptX + optBxSdLen)) &&
			(y >= ((menuLineH * 2) + tpMenuYBxOffst)) && (y <= ((menuLineH * 2) + tpMenuYBxOffst + optBxSdLen))) {
			itemInBounds = 2;
		} else if ((x >= ltOptX) && (x <= (ltOptX + optBxSdLen)) &&
			(y >= ((menuLineH * 3) + tpMenuYBxOffst)) && (y <= ((menuLineH * 3) + tpMenuYBxOffst + optBxSdLen))) {
			itemInBounds = 3;
		} else if ((x >= ltOptX) && (x <= (ltOptX + optBxSdLen)) &&
			(y >= ((menuLineH * 4) + tpMenuYBxOffst)) && (y <= ((menuLineH * 4) + tpMenuYBxOffst + optBxSdLen))) {
			itemInBounds = 4;

		// RIGHT COLUMN
		// Check for bounds of decrease bomb frequency button
		} else if (((x >= rtOptDcrsBtnX) && ( x <= (rtOptDcrsBtnX + optArrwW))) &&
			((y >= (menuLineH + rtOptArrwYOffst)) && (y <= (menuLineH + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 10;
		// Check for bounds of increase bomb frequency button
		} else if (((x >= rtOptIncrsBtnX) && ( x <= (rtOptIncrsBtnX + optArrwW))) &&
				((y >= (menuLineH + rtOptArrwYOffst)) && (y <= (menuLineH + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 11; 

		// Check for bounds of decrease starting level button
		} else if (((x >= rtOptDcrsBtnX) && ( x <= (rtOptDcrsBtnX + optArrwW))) &&
				((y >= ((menuLineH * 2) + rtOptArrwYOffst)) && (y <= ((menuLineH * 2) + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 12;
		// Check for bounds of increase starting level button
		} else if (((x >= rtOptIncrsBtnX) && ( x <= (rtOptIncrsBtnX + optArrwW))) &&
				((y >= ((menuLineH * 2) + rtOptArrwYOffst)) && (y <= ((menuLineH * 2) + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 13;

		// Decrease sound effects volume
		} else if (((x >= rtOptDcrsBtnX) && ( x <= (rtOptDcrsBtnX + optArrwW))) &&
			((y >= ((menuLineH * 3) + rtOptArrwYOffst)) && (y <= ((menuLineH * 3) + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 14;
		// Increase sound effects volume
		} else if (((x >= rtOptIncrsBtnX) && ( x <= (rtOptIncrsBtnX + optArrwW))) &&
				((y >= ((menuLineH * 3) + rtOptArrwYOffst)) && (y <= ((menuLineH * 3) + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 15;

		// Decrease music volume
		} else if (((x >= rtOptDcrsBtnX) && ( x <= (rtOptDcrsBtnX + optArrwW))) &&
				((y >= ((menuLineH * 4) + rtOptArrwYOffst)) && (y <= ((menuLineH * 4) + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 16;
		// Increase music volume
		} else if (((x >= rtOptIncrsBtnX) && ( x <= (rtOptIncrsBtnX + optArrwW))) &&
				((y >= ((menuLineH * 4) + rtOptArrwYOffst)) && (y <= ((menuLineH * 4) + rtOptArrwYOffst + optArrwH)))) {
			itemInBounds = 17;

		// Return to Main Menu
		} else if (((x >= (scrMidX - (metricsBtMMstr / 2) - 10)) && ( x <= (scrMidX - (metricsBtMMstr / 2) + metricsBtMMstr + 10))) &&
			((y >= tpMenuY + (menuLineH * 6)) && (y <= tpMenuY + (menuLineH * 6) + 40))) {
			itemInBounds = 99;
		}
//		g2d.fill3DRect(scrMidX - (metricsBtMMstr / 2) - 10,  cfgMenuY + (menuLineH * 8), metricsBtMMstr + 20, 40, true);

		return itemInBounds;
	}
	
	public void setConfig(PropertiesManager config) {
		this.propMgr = config;
	}

	private String getBombFrequency() {
		// TODO Auto-generated method stub
		String strBombFrequency;

		switch (Integer.parseInt(propMgr.getProperty("bombFrequency"))) {
			case 1:		strBombFrequency = "Constant";			break;
			case 2:		strBombFrequency = "Every 1-5 secs";	break;
			case 3:		strBombFrequency = "Every 5-10 secs";	break;
			case 4:		strBombFrequency = "Every 10-20 secs";	break;
			case 5:		strBombFrequency = "Every 20-30 secs";	break;
			default:	strBombFrequency = "Every 1-5 secs";	propMgr.setProperty("bombFrequency", "3");	break;
		}
		
		return strBombFrequency;
	}

}
