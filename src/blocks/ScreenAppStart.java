package blocks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;

public class ScreenAppStart implements IScreen {

//	private Image imgBackground;
	private Image imgIntro;
	private DisplayManager scrMgr;
	private FontManager fntMgr;
	private Graphics2D g2d;

	private int scrMidX;

	
	public ScreenAppStart(DisplayManager scrMgr, FontManager fntMgr) {
		this.scrMgr = scrMgr;
		this.fntMgr = fntMgr;

		loadImages();

		scrMidX = scrMgr.getWidth() / 2;
	}
	
	@Override
	public void loadImages() {
		// TODO Auto-generated method stub
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "background.jpg")).getImage();
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "brickwall.jpg")).getImage();
		imgIntro = new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "intro.jpg")).getImage();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		g2d = scrMgr.getGraphics();

		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, scrMgr.getWidth(), scrMgr.getHeight());	// Gamefield
//		g2d.drawImage(imgBackground, 0, 0, null);					// Brick wall
		g2d.drawImage(imgIntro, scrMidX - (imgIntro.getWidth(null) / 2), 100, null);
		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.STR_AUTHOR_INTRO, (scrMidX / 2) - (fntMgr.getMetrics().stringWidth(Constants.STR_AUTHOR_INTRO) / 2), 700);
		g2d.drawString(Constants.STR_APP_VERSION, (scrMgr.getWidth() - (scrMidX / 2)) - (fntMgr.getMetrics().stringWidth(Constants.STR_APP_VERSION) / 2), 700);

		g2d.dispose();
		scrMgr.update();
	}

	@Override
	public int checkMousePos(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}

}
