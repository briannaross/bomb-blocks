package blocks;

import java.awt.Image;
import java.util.Timer;
import java.util.TimerTask;

public class Sprite {

	private Animation animation;
	
	private Timer t;
	
	private float x;
	private float y;
	private float vx;
	private float vy;
	
	private int interval;
	
	//constructor
	public Sprite (Animation a) {
		this.animation = a;
		interval = 100;

		t = new Timer();
		t.schedule(new TimerTask() {
			public void run() { 
				x += vx * interval;
				y += vy * interval;
				animation.update(interval);
			}
		}, interval, interval);
	}
	
	//change position
	public void update(long timePassed) {
		x += vx * timePassed;
		y += vy * timePassed;
		animation.update(timePassed);
	}
	

	//get x position
	public float getX() {
		return x;
	}
	
	//get y position
	public float getY() {
		return y;
	}
	
	//set sprite x position
	public void setX(float x) {
		this.x = x;
	}
	
	//set sprite y position
	public void setY(float y) {
		this.y = y;
	}
	
	//get sprite width
	public int getWidth() {
		return animation.getImage().getWidth(null);
	}
	
	//get sprite height
	public int getHeight() {
		return animation.getImage().getHeight(null);
	}

	//get horizontal velocity
	public float getVelocityX() {
		return vx;
	}

	//get vertical velocity
	public float getVelocityY() {
		return vy;
	}

	//set horizontal velocity
	public void setVelocityX(float vx) {
		this.vx = vx;
	}

	//set vertical velocity
	public void setVelocityY(float vy) {
		this.vy = vy;
	}

	//get sprite/images
	public Image getImage() {
		return animation.getImage();
	}
	
	public int getNumCycles() {
		return animation.getNumCycles();
	}
	
	public void resetCycles() {
		animation.resetCycles();
	}
}
