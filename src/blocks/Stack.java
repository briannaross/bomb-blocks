package blocks;

public class Stack {

	private transient int theStack[][];    // The stack [x][] is rows top to bottom, [][y] is columns left to right (e.g. [0][0] is top-left, [25][10] is bottom-right)
	private transient int totRowsCleared = 0;
	private transient int rowsClrdThisTurn = 0;


	// Constructor
	public Stack() {
		theStack = new int[Constants.STACKWIDTH][Constants.STACKHEIGHT];
		
		for (int x = 0; x < Constants.STACKWIDTH; x++) {
			for (int y = 0; y < Constants.STACKHEIGHT; y++) {
				theStack[x][y] = Constants.TILEEMPTY;
			}
		}
	}

	public boolean checkIfBlockResting(final int block[][]) {
		int tileX = 0;
		int tileY = 0;
		boolean resting = false;

		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
			tileX = block[i][0];
			tileY = block[i][1];

			if (tileY == (Constants.STACKHEIGHT - 1)) {
				resting = true;
			} else if ((theStack[tileX][tileY + 1] != Constants.TILEEMPTY) && (theStack[tileX][tileY + 1] != Constants.TILEEXPLOSION)) {
				resting = true;
			}
		}

		return resting;
	}
	
	public boolean checkIfTileResting(final int tileX, final int tileY) {
		boolean resting = false;

		if (tileY == (Constants.STACKHEIGHT - 1)) {
			resting = true;
		} else if ((theStack[tileX][tileY + 1] != Constants.TILEEMPTY) && (theStack[tileX][tileY + 1] != Constants.TILEEXPLOSION)) {
			resting = true;
		}

		return resting;
	}


	public void putBlockOnStack(final int block[][], final int blocknum) {
		int tileX = 0;
		int tileY = 0;

		for (int i = 0; i < Constants.TILESINBLOCK; i++) {
			tileX = block[i][0];
			tileY = block[i][1];

			theStack[tileX][tileY] = blocknum;
		}
		
		calcNumFullRows();
	}

	
	private void calcNumFullRows() {
		boolean incompleterow = false;

		for (int y = 0; y < Constants.STACKHEIGHT; y++) {
			for (int x = 0; x < Constants.STACKWIDTH; x++) {
				if ((theStack[x][y] == Constants.TILEEMPTY) || (theStack[x][y] == Constants.TILEEXPLOSION)) {
					incompleterow = true;
				}
			}

			if (!incompleterow) {
				for (int x = 0; x < Constants.STACKWIDTH; x++) {
					theStack[x][y] = 10; // 10 is the number for the animated tile, used when clearing the row
				}
				rowsClrdThisTurn++;
			}
			incompleterow = false;
		}

		totRowsCleared = totRowsCleared + rowsClrdThisTurn; 
	}

	public int getRowsClearedThisTurn() {
		return rowsClrdThisTurn;
	}
	
	public void resetFullRowCount() {
		rowsClrdThisTurn = 0;
	}
	
	public int getTotRowsCleared() {
		return totRowsCleared;
	}

	public void deleteFullRows() {
		boolean rownotfull = false;

		for (int y = (Constants.STACKHEIGHT - 1); y >= 0  ; y--) {
			for (int x = 0; x < Constants.STACKWIDTH; x++) {
				if ((theStack[x][y] == Constants.TILEEMPTY) || (theStack[x][y] == Constants.TILEEXPLOSION)) {
					rownotfull = true;
				}
			}
			
			if (!rownotfull) {
				for (int dropY = y; dropY > 0; dropY--) {
					for (int x = 0; x < Constants.STACKWIDTH; x++) {
						theStack[x][dropY] = theStack[x][dropY - 1]; // Move row above current down one line
					}
				}

				for (int x = 0; x < Constants.STACKWIDTH; x++) { // Clear the top row
					theStack[x][0] = Constants.TILEEMPTY;
				}

				y++; // Increment x because it has to recheck the line it's just cleared, as this has now been filled with the tiles from the row above.
			}
			rownotfull = false;
		}
	}
	
	public void collapseEmptyRows() {
		boolean rowEmpty = true;
		boolean rowAboveEmpty = true;

		for (int y = (Constants.STACKHEIGHT - 1); y >= 0  ; y--) {
			for (int x = 0; x < Constants.STACKWIDTH; x++) {
				if (theStack[x][y] <= Constants.MAXTILES) {
					rowEmpty = false;
				}
				if (y > 1) {
					if (theStack[x][y - 1] <= Constants.MAXTILES) {
						rowAboveEmpty = false;
					}
				}
			}
			
			if ((rowEmpty) && (!rowAboveEmpty)) {
				for (int dropY = y; dropY > 0; dropY--) {
					for (int x = 0; x < Constants.STACKWIDTH; x++) {
						theStack[x][dropY] = theStack[x][dropY - 1]; // Move row above current down one line
					}
				}

				for (int x = 0; x < Constants.STACKWIDTH; x++) { // Clear the top row
					theStack[x][0] = Constants.TILEEMPTY;
				}

				y++; // Increment x because it has to recheck the line it's just cleared, as this has now been filled with the tiles from the row above.
			}
			rowEmpty = true;
			rowAboveEmpty = true;
		}
	}
	
	public int[][] getStack() {
		return theStack.clone();
	}

	public void deleteTileAtLocation(final int xDelete, final int yDelete) {
		theStack[xDelete][yDelete] = Constants.TILEEMPTY;
	}
	
	public void explodeTileAtLocation(final int minX, final int maxX, final int minY, final int maxY) {
		for (int i = minX; i <= maxX; i++) {
			for (int j = minY; j <= maxY; j++) {
				theStack[i][j] = Constants.TILEEXPLOSION;
			}
		}
	}

	
	public void deleteExplodedTiles() {
		for (int x = 0; x < Constants.STACKWIDTH; x++) {
			for (int y = 0; y < Constants.STACKHEIGHT; y++) {
				if (theStack[x][y] == Constants.TILEEXPLOSION) {
					theStack[x][y] = Constants.TILEEMPTY;
				}
			}
		}
	}
	
}
