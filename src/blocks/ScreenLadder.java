package blocks;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.sql.SQLException;

import javax.swing.ImageIcon;

public class ScreenLadder implements IScreen {

	private DisplayManager scrMgr;
	private FontManager fntMgr;
	private Ladder ladder;
	private Graphics2D g2d;
//	private Image imgBackground;
	private Image imgIntro;
	
	private int scrMidX;

	
	public ScreenLadder(DisplayManager scrMgr, FontManager fntMgr, Ladder ladder) {
		this.scrMgr = scrMgr;
		this.fntMgr = fntMgr;
		this.ladder = ladder;

		loadImages();

		scrMidX = scrMgr.getWidth() / 2;
	}
	
	@Override
	public void loadImages() {
		// TODO Auto-generated method stub
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "background.jpg")).getImage();
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "brickwall.jpg")).getImage();
		imgIntro			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "intro.jpg")).getImage();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		g2d = scrMgr.getGraphics();

		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, scrMgr.getWidth(), scrMgr.getHeight()); 		// Gamefield
//		g2d.drawImage(imgBackground, 0, 0, null);						// Brick wall
		g2d.drawImage(imgIntro, scrMidX - 160, 50, 320, 240, null);	// Intro graphic
		g2d.setColor(Color.WHITE);
		g2d.drawString("Ladder", scrMidX - ((fntMgr.getMetrics().stringWidth("Ladder") / 2)), 350);

		for (int i = 0; i < 10; i++) {
			try {
				if (ladder.getLadder()[i][0] != null) {
					g2d.drawString(ladder.getLadder()[i][0], scrMidX - 250, 400 + (i * 30));	// Player name
					g2d.drawString(ladder.getLadder()[i][1], scrMidX - 50, 400 + (i * 30));		// Score
					g2d.drawString(ladder.getLadder()[i][2], scrMidX + 150, 400 + (i * 30));	// Date
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		g2d.setColor(Color.darkGray);
		g2d.fill3DRect((scrMidX - (fntMgr.getMetrics().stringWidth(Constants.MENUTXT_BACKTOMM) / 2)) - 10, 700, ((scrMidX - (scrMidX - (fntMgr.getMetrics().stringWidth(Constants.MENUTXT_BACKTOMM) / 2))) * 2) + 20, 40, true);
		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.MENUTXT_BACKTOMM, (scrMidX - (fntMgr.getMetrics().stringWidth(Constants.MENUTXT_BACKTOMM) / 2)), 730);
	
		g2d.dispose();
		scrMgr.update();
	}

	@Override
	public int checkMousePos(int x, int y) {
		// TODO Auto-generated method stub
		return 0;
	}

}
