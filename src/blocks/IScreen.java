package blocks;

public interface IScreen {
	void loadImages();
	void draw();
	int checkMousePos(int mouseX, int mouseY);
}
