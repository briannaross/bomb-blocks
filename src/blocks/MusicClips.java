package blocks;

public final class MusicClips {
    private MusicClips() {
    	throw new AssertionError();
    }  // Prevents instantiation

	public static final MediaManager MUS_MENUS = new MediaManager();
	public static final MediaManager MUS_GAME[] = {new MediaManager(), new MediaManager(), new MediaManager(), new MediaManager(), new MediaManager(),
												   new MediaManager(), new MediaManager()};
}
