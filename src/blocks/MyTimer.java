package blocks;

import java.util.Random;

public class MyTimer {

	private transient final long origInterval;

	private transient long startingTime;
	private transient long cumTime;
	private transient long timePassed;
	private transient long curInterval;
	private transient long pausedTime;
	private transient long pauseStart;
	
	private transient boolean boolTimerActive;

	
	public MyTimer(final int orgIntrvl) {
		origInterval = orgIntrvl;
		
		init();
	}
	
	// Overloaded constructor sets the interval to a random number (time) between the min and max values.
	public MyTimer(final int min, final int max) {
		final int seed = max - min;
		final Random randomizer = new Random();
		origInterval = randomizer.nextInt(seed) + 1 + min;
		
		init();
	}
	
	private void init() {
		boolTimerActive = true;
		startingTime = System.currentTimeMillis();
		cumTime = 0;
		timePassed = 0;
		curInterval = origInterval;
		pausedTime = 0;
		pauseStart = 0;
	}
	
	public boolean isIntervalReached() {
		boolean boolIntrvlRchd;
		
		update();
		if ((System.currentTimeMillis() - startingTime) > curInterval) {
			boolIntrvlRchd = true;
		} else {
			boolIntrvlRchd =  false;
		}
		
		return boolIntrvlRchd;
	}

	public long getTimePassed() {
		update();
		return timePassed;
	}
	
	private void update() {
		timePassed = System.currentTimeMillis() - cumTime;
		cumTime += timePassed;
	}
	
	public void reset() {
		startingTime = System.currentTimeMillis();
		cumTime = 0;
	}
	
	public long getStartingTime() {
		return startingTime;
	}
	
	public long getInterval() {
		return curInterval;
	}
	
	public void setInterval(final long intrvl) {
		curInterval = intrvl;
	}
	
	public boolean isTimerActive() {
		return boolTimerActive;
	}
	
	public void pause() {
		pauseStart = System.currentTimeMillis();
	}
	
	public void resume() {
		pausedTime = pausedTime + (System.currentTimeMillis() - pauseStart);
		pauseStart = 0;
	}
	

	public void close() {
		boolTimerActive = false;
	}
	
}
