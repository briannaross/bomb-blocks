package blocks;

public final class Constants {
    private Constants() {
    	throw new AssertionError();
    }  // Prevents instantiation

	public static final int STACKHEIGHT = 25;
	public static final int STACKWIDTH = 10;
	public static final int TILEBONUS = 80;
	public static final int TILEEXPLOSION = 90;
	public static final int TILEEMPTY = 99;
	public static final int MAXTILES = 7;
	public static final int INTERVALDROPBLOCK = 1000;
	public static final int TILESINBLOCK = 4;

	public static final String PATH_FONTS = "/resource/fonts/";
	public static final String PATH_IMGS = "/resource/images/";
	public static final String PATH_SNDS = "/resource/sounds/";
	public static final String PATH_MUSIC = "/resource/music/";
	public static final String PATH_CONFIG_DATA = "data/config.cfg";
	public static final String PATH_LADDER_DATA = "data/ladder.dat";
	
	public static final String STR_AUTHOR_INTRO = "A game by Andrew Ross";
	public static final String STR_APP_VERSION = "version 0.1a";
	public static final String STR_TRUE = "true";
	public static final String STR_FALSE = "false";
	public static final String BOMBTYPE_LEVELLER = "Leveller";
	public static final String MENUTXT_BACKTOMM = "F10 - Back to Main Menu";
	public static final String MENUTXT_STARTGAME = "F2 - Start game";
	public static final String MENUTXT_LADDER = "F3 - Ladder";
	public static final String MENUTXT_CONFIG = "F4 - Config";
	public static final String MENUTXT_QUIT = "F10 - Quit";
	public static final String BTNTXT_HELP = "F1 - Help";
	public static final String BTNTXT_PAUSE_GAME = "F9 - Pause Game";
	public static final String BTNTXT_QUIT_GAME = "F10 - Quit Game";
	public static final String PROP_DSP_TM_ELPSD = "displayTimeElapsed";
	public static final String PROP_BMBS_ACTVTD = "bombsActivated";
	public static final String PROP_DSP_NXT_BLCK = "displayNextBlock";
	public static final String PROP_INSTNT_DROP = "instantDrop";
	public static final String PROP_BMB_FRQNCY = "bombFrequency";
	public static final String PROP_STRTNG_LVL = "startingLevel";
	public static final String PROP_SFX_VOL = "soundEffectsVolume";
	public static final String PROP_MUS_VOL = "musicVolume";
	public static final String FONT_NAME = "Mecha_Bold.ttf";

	public static enum GameStates { APPSTART, MAINMENU, CONFIG, LADDER, PREGAME, STARTGAME, PLAYGAME, FASTHELP, PAUSE, ENDGAME, POSTGAME, APPEND, EXIT, NONE };
	public static enum BlockMoves { LEFT, RIGHT, DOWN, ROTATE, NONE };
}
