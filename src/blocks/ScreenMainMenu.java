package blocks;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;

public class ScreenMainMenu implements IScreen {

//	private Image imgBackground;
	private Image imgIntro;
	private Graphics2D g2d;
	private DisplayManager scrMgr;

	private int mainMenuX;
	private int scrMidX;


	public ScreenMainMenu(DisplayManager scrMgr, FontManager fntMgr) {
		this.scrMgr = scrMgr;
		
		scrMidX = scrMgr.getWidth() / 2;
		FontMetrics metrics = fntMgr.getMetrics();
		mainMenuX = (scrMidX - (metrics.stringWidth(Constants.MENUTXT_STARTGAME) / 2));
		
		loadImages();
	}
	
	@Override
	public void loadImages() {
		// TODO Auto-generated method stub
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "background.jpg")).getImage();
//		imgBackground 		= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "brickwall.jpg")).getImage();
		imgIntro			= new ImageIcon(this.getClass().getResource(Constants.PATH_IMGS + "intro.jpg")).getImage();
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		g2d = scrMgr.getGraphics();

		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, scrMgr.getWidth(), scrMgr.getHeight()); // Gamefield

		g2d.drawImage(imgIntro, scrMidX - 160, 100, 320, 240, null);

		g2d.setColor(Color.darkGray);
		g2d.fill3DRect(mainMenuX - 10, 370, ((scrMidX - mainMenuX) * 2) + 20, 40, true);
		g2d.fill3DRect(mainMenuX - 10, 420, ((scrMidX - mainMenuX) * 2) + 20, 40, true);
		g2d.fill3DRect(mainMenuX - 10, 470, ((scrMidX - mainMenuX) * 2) + 20, 40, true);
		g2d.fill3DRect(mainMenuX - 10, 520, ((scrMidX - mainMenuX) * 2) + 20, 40, true);

		g2d.setColor(Color.WHITE);
		g2d.drawString(Constants.MENUTXT_STARTGAME, mainMenuX, 400);
		g2d.drawString(Constants.MENUTXT_LADDER, mainMenuX, 450);
		g2d.drawString(Constants.MENUTXT_CONFIG, mainMenuX, 500);
		g2d.drawString(Constants.MENUTXT_QUIT, mainMenuX, 550);

		g2d.dispose();
		scrMgr.update();
	}

	@Override
	public int checkMousePos(int mouseX, int mouseY) {
		// TODO Auto-generated method stub
		int itemInBounds = 0;
		
		if ((mouseX >= (mainMenuX - 10) && (mouseX <= (mainMenuX - 10) + ((scrMidX - mainMenuX) * 2) + 20) &&
			(mouseY >= 370) && (mouseY <= 370 + 40))) {
			itemInBounds = 1;
		} else if ((mouseX >= (mainMenuX - 10) && (mouseX <= (mainMenuX - 10) + ((scrMidX - mainMenuX) * 2) + 20) &&
			(mouseY >= 470) && (mouseY <= 470 + 40))) {
			itemInBounds = 2;
		} else if ((mouseX >= (mainMenuX - 10) && (mouseX <= (mainMenuX - 10) + ((scrMidX - mainMenuX) * 2) + 20) &&
			(mouseY >= 420) && (mouseY <= 420 + 40))) {
			itemInBounds = 3;
		} else if ((mouseX >= (mainMenuX - 10) && (mouseX <= (mainMenuX - 10) + ((scrMidX - mainMenuX) * 2) + 20) &&
			(mouseY >= 520) && (mouseY <= 520 + 40))) {
			itemInBounds = 4;
		}
		
		return itemInBounds;
	}

}
