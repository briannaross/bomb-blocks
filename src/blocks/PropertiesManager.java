package blocks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {

	private transient final Properties propFile;

	// Constructor
	public PropertiesManager() throws IOException {
		final File file = new File(Constants.PATH_CONFIG_DATA);
		if (!file.isFile())
			{ file.createNewFile();	}

		propFile = new Properties();
		propFile.load(new FileInputStream(Constants.PATH_CONFIG_DATA));
	}

	public boolean checkForKey(final String key) {
		return propFile.containsKey(key);
	}
	
	public String getProperty(final String key) {
		return propFile.getProperty(key);
	}

	public void setProperty(final String key, final String value) {
		propFile.setProperty(key, value);
	}

	public void writeProperties() {
		try {
			propFile.store(new FileOutputStream(Constants.PATH_CONFIG_DATA), "Bomb Blocks Configuration File");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
