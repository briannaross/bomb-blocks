//////////////////////////////////////////////////////////////////////////////////////
//
// FILE:		Core.java 
// AUTHOR:		Andrew Ross
// DATE:		14/5/2012
// LAST UPDATE: 20/6/2012
// PURPOSE:		Core logic for Bomb Blocks program
//
/////////////////////////////////////////////////////////////////////////////////////


package blocks;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

public class Core implements KeyListener, MouseListener, MouseMotionListener {

	// Start this thang.
	public static void main(final String[] args) {
		final Core core = new Core();
		core.run();
		System.exit(0);
	}

	
	/////////////////
	// Initialisation
	/////////////////
	
	private transient DisplayManager scrMgr;
	private transient Score score;
	private transient Block currentBlock;
	private transient Block nextBlock;
	private transient Stack stack;
	private transient AbstractBomb bomb;
	private transient MyTimer timerDropBlock;
	private transient MyTimer timerNextBomb;
	private transient Ladder ladder;
	private transient PropertiesManager propMgr;

	private transient Constants.GameStates gameState = Constants.GameStates.APPSTART;
	private transient Constants.BlockMoves blockMove = Constants.BlockMoves.NONE;

	private	transient String pName = "";
	private transient String strAlrtMsg = "";
//	private String strDbgMsg = "";

	private transient int intrvlNxtBmbMin = 0;
	private transient int intrvlNxtBmbMax = 0;
	private transient int mouseX = 0;
	private transient int mouseY = 0;

	private transient long timeElapsed = 0;

	
	
	//////////////////////
	// State Manager
	//////////////////////

	public void run() {
		// Initialise various bits and pieces needed to get this baby started.
		initConfig();
		initLadder();
		initAudio();
		initScreen();

		// State Manager. Run the method depending on the current game state.
		while (!gameState.equals(Constants.GameStates.EXIT)) {
			switch(gameState) {
				case APPSTART:		runAppStart();		break;
				case MAINMENU:		runMainMenu();		break;
				case PREGAME:		runPregame();		break;
				case PLAYGAME:		runPlaygame();		break;
				case POSTGAME:		runPostgame();		break;
				case LADDER:		runLadder();		break;
				case CONFIG:		runConfig();		break;
				case APPEND:		runAppEnd();		break;
				default:			break;
			}
		}

		// Restore the operating system screen.
		scrMgr.restoreScreen();
		// Write player configuration out to the properties file.
		propMgr.writeProperties();
	}
	

	public void initConfig() {
		// Initialise config (properties) file. Set default value where one is not present.
		try {
			propMgr = new PropertiesManager();
			if (!propMgr.checkForKey(Constants.PROP_BMBS_ACTVTD))
				{ propMgr.setProperty(Constants.PROP_BMBS_ACTVTD, Constants.STR_TRUE); }
			if (!propMgr.checkForKey(Constants.PROP_DSP_NXT_BLCK))
				{ propMgr.setProperty(Constants.PROP_DSP_NXT_BLCK, Constants.STR_TRUE); }
			if (!propMgr.checkForKey(Constants.PROP_DSP_TM_ELPSD))
				{ propMgr.setProperty(Constants.PROP_DSP_TM_ELPSD, Constants.STR_TRUE); }
			if (!propMgr.checkForKey(Constants.PROP_INSTNT_DROP))
				{ propMgr.setProperty(Constants.PROP_INSTNT_DROP, Constants.STR_TRUE); }
			if (!propMgr.checkForKey(Constants.PROP_BMB_FRQNCY))
				{ propMgr.setProperty(Constants.PROP_BMB_FRQNCY, "2"); }
			if (!propMgr.checkForKey(Constants.PROP_STRTNG_LVL))
				{ propMgr.setProperty(Constants.PROP_STRTNG_LVL, "1"); }
			if (!propMgr.checkForKey(Constants.PROP_SFX_VOL))
				{ propMgr.setProperty(Constants.PROP_SFX_VOL, "5"); }
			if (!propMgr.checkForKey(Constants.PROP_MUS_VOL))
				{ propMgr.setProperty(Constants.PROP_MUS_VOL, "2"); }
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

		
	public void initLadder() {
		// Initialise ladder. The ladder is loaded from a text file into a SQLite database.
		// Why SQLite and not a two-dimensional array or a list? Because it was easier for me. Might not be efficient but it works!
		try {
			ladder = new Ladder();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		

	public void initAudio() {
		// Initialise sound clips.
		SoundClips.SND_RSTNG_BLCK.load(Constants.PATH_SNDS + "restBlock.wav");
		SoundClips.SND_CLR_ROW.load(Constants.PATH_SNDS + "clearRow.wav");
		SoundClips.SND_EXPLSN.load(Constants.PATH_SNDS + "explosion.wav");

		setSfxVolumes();

		MusicClips.MUS_MENUS.load("Frozen.mp2");
		MusicClips.MUS_GAME[0].load("SteelCan.mp2");
		MusicClips.MUS_GAME[1].load("DasElect.mp2");
		MusicClips.MUS_GAME[2].load("PowerHun.mp2");
		MusicClips.MUS_GAME[3].load("CreepAlo.mp2");
		MusicClips.MUS_GAME[4].load("Psykick.mp2");
		MusicClips.MUS_GAME[5].load("AngryMod.mp2");
		MusicClips.MUS_GAME[6].load("OrangeRa.mp2");
	}


	private void setSfxVolumes() {
		// TODO Auto-generated method stub
		int vol = Integer.parseInt(propMgr.getProperty(Constants.PROP_SFX_VOL));

		SoundClips.SND_CLR_ROW.setVolume(vol);
		SoundClips.SND_EXPLSN.setVolume(vol);
		SoundClips.SND_RSTNG_BLCK.setVolume(vol);
	}

	
	private void setMusVolume() {
		// TODO Auto-generated method stub
		float vol = Float.valueOf(propMgr.getProperty(Constants.PROP_MUS_VOL)) / 10;
		
		MusicClips.MUS_MENUS.setVolume(vol);
		for (int i = 0; i < MusicClips.MUS_GAME.length; i++) {
			MusicClips.MUS_GAME[i].setVolume(vol);
		}
	}

	
	public void initScreen() {
		// Initialise graphics
		scrMgr = new DisplayManager();
		scrMgr.setFullScreen(scrMgr.findFirstCompatibleMode());

		final FontManager fntMgr = new FontManager(scrMgr, Constants.FONT_NAME);

		// Set the font.
		scrMgr.getFullScreenWindow().setFont(fntMgr.getFont());
		
		// Attach listeners.
		scrMgr.getFullScreenWindow().setFocusTraversalKeysEnabled(false);
		scrMgr.getFullScreenWindow().addKeyListener(this);
		scrMgr.getFullScreenWindow().addMouseListener(this);
		scrMgr.getFullScreenWindow().addMouseMotionListener(this);

		// Initialise screens.
		Screens.screenAppStart = new ScreenAppStart(scrMgr, fntMgr);
		Screens.screenMainMenu = new ScreenMainMenu(scrMgr, fntMgr);
		Screens.screenConfig   = new ScreenConfig(scrMgr, fntMgr, propMgr);
		Screens.screenLadder   = new ScreenLadder(scrMgr, fntMgr, ladder);
		Screens.screenGame     = new ScreenGame(scrMgr, fntMgr);
		Screens.screenAppEnd   = new ScreenAppEnd(scrMgr, fntMgr);
	}

	

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		APPSTART
	// Purpose:		Displays the start-up animation.
    // Exit state:	MAINMENU. It will do this either after the animation completes or user pressed key,
	//				whichever comes first.
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void runAppStart() {
		// Display the starting splash screen for five seconds or until a player clicks a mouse button or presses a key.
		final MyTimer timer = new MyTimer(5000);
		
		Screens.screenAppStart.draw();
		MusicClips.MUS_MENUS.fadeIn(Float.valueOf(propMgr.getProperty(Constants.PROP_MUS_VOL)) / 10);
		
		while (gameState == Constants.GameStates.APPSTART) {
			if (timer.isIntervalReached()) {
				gameState = Constants.GameStates.MAINMENU;
				timer.reset();
			}
			Screens.screenAppStart.draw();
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		MAINMENU
    // Purpose:		Displays the main menu.
	// Exit state:	Depends on key pressed.
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void runMainMenu() {
		Screens.screenMainMenu.draw();
		if (!MusicClips.MUS_MENUS.isRunning()) {
			MusicClips.MUS_MENUS.play();
			MusicClips.MUS_MENUS.fadeIn(Float.valueOf(propMgr.getProperty(Constants.PROP_MUS_VOL)) / 10);
		}

		// Display the main menu. State is change on mouse button click or key press.
		while (gameState == Constants.GameStates.MAINMENU) {
			Screens.screenMainMenu.draw();
		}
	}

	
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		PREGAME
    // Purpose:		Displays 'Get Ready!" message before the game starts.
	// Exit state:	PLAYGAME, after timeout.
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void runPregame() {
		// Display the pre-game 'Get ready!' message for one second.
		final MyTimer timer = new MyTimer(1000);
		strAlrtMsg = "Get ready!";
		
		MusicClips.MUS_MENUS.fadeOut();

		
		while (gameState == Constants.GameStates.PREGAME) {
			if (timer.isIntervalReached()) {
				gameState = Constants.GameStates.PLAYGAME;
				timer.reset();
				strAlrtMsg = "";
			}

			Screens.screenGame.start();
			Screens.screenGame.draw();
			Screens.screenGame.drawAlertMsg(strAlrtMsg);
			Screens.screenGame.finalise();
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		POSTGAME
    // Purpose:		Displays 'Game Over!' message after the game ends.
	// Exit state:	MAINMENU, after timeout.
	//				Will exit to high score ladder when the ladder section is completed.
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public void runPostgame() {
		// Display the post-game 'Game over!' message for one second. Display the final game screen in the background.
		final MyTimer timer = new MyTimer(2500);
		strAlrtMsg = "Game over!";

		while (!timer.isIntervalReached()) {
			Screens.screenGame.start();
			Screens.screenGame.draw();
			Screens.screenGame.drawExplosion(stack);
			Screens.screenGame.drawStack(stack);
			Screens.screenGame.drawBlockPreview(nextBlock.getAllBlocks(), nextBlock.getBlocknum());
			Screens.screenGame.drawStats(stack, score, propMgr, timeElapsed);
			Screens.screenGame.drawAlertMsg(strAlrtMsg);
			Screens.screenGame.finalise();
		}

		// Do processing for checking and entering a high score.
		enterHighScore();

		// Clear alert
		strAlrtMsg = "";
	}

	private void enterHighScore() {
		boolean isHighScore = false;

		// Check the ladder to see if the players score is a new high score (well top ten anyway).
		try {
			isHighScore = ladder.checkForHighScore(score.getScore());
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// Display dialog to enter a player name for the high score ladder (if applicable).
		if (isHighScore) {
			strAlrtMsg = "High score! Enter your name:";

			// Draw the final game field state in the background.
			while (gameState == Constants.GameStates.POSTGAME) {
				Screens.screenGame.start();
				Screens.screenGame.draw();
				Screens.screenGame.drawExplosion(stack);
				Screens.screenGame.drawStack(stack);
				Screens.screenGame.drawBlockPreview(nextBlock.getAllBlocks(), nextBlock.getBlocknum());
				Screens.screenGame.drawStats(stack, score, propMgr, timeElapsed);
				// Draw the high score stuff in the foreground in the middle of the stack area.
				if ("".equals(pName)) {
					Screens.screenGame.drawAlertMsg(strAlrtMsg);
				} else {
					Screens.screenGame.drawAlertMsg(pName);
				}
				Screens.screenGame.finalise();
			}

			// Insert the new high score into the ladder, catch any exception.
			try {
				ladder.setNewHighScore(pName, score.getScore());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Display the ladder.
			gameState = Constants.GameStates.LADDER;

		} else {
			// Take the player back to the main menu.
			gameState = Constants.GameStates.MAINMENU;
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		LADDER
    // Purpose:		Displays the all-time high score ladder.
	// Exit state:	MAINMENU, on any key press.
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public void runLadder() {
		// Display the high score ladder. State is change on mouse button click or key press.
		while (gameState == Constants.GameStates.LADDER) {
			Screens.screenLadder.draw();
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		CONFIG
    // Purpose:		Display configuration options and allows user to edit them.
	// Exit state:	MAINMENU, on any key press.
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void runConfig() {
		// Display the config menu. Keep passing the propMgr object to the screenConfig object so that it displays updated config options in real time.
		while (gameState == Constants.GameStates.CONFIG) {
			Screens.screenConfig.setConfig(propMgr);
			Screens.screenConfig.draw();
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State:		APPEND
    // Purpose:		Run any animation or other screens once the user exits the application.
	// Exit state:	EXIT, which exits the program.
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public void runAppEnd() {
		// Display the application end screen for 1.5 seconds.
		final MyTimer timer = new MyTimer(1500);

		while (gameState == Constants.GameStates.APPEND) {
			if (timer.isIntervalReached()) {
				gameState = Constants.GameStates.EXIT;
				timer.reset();
			}
			Screens.screenAppEnd.draw();
		}

		// Save the ladder to a file. Using SQL which require catching exceptions.
		try {
			ladder.saveLadder();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		if (!MusicClips.MUS_MENUS.isRunning()) {
			MusicClips.MUS_MENUS.stop();
//		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////
	// State: PLAYGAME
    // This is the actual game.
	/////////////////////////////////////////////////////////////////////////////////////////////////////

	public void runPlaygame() {

		// Initialisation
		
		// Set the clear row animation to false so that it doesn't display yet.
		boolean boolClearRowsAnim = false;
		// Set the game start time to the current time.
		final long timeGameStart = System.currentTimeMillis();
		
		long pauseTime = 0;

		int musicTrack = 0;
		
		// Create new block, stack, and score.
		nextBlock = new Block();
		stack = new Stack();
		score = new Score();
		// Set bomb to null; the bomb timer will determine when it is to be created.
		bomb = null;

		// Set the frequency with which a new bomb is created.
		setBombFrequency();

		// Set the starting level according to what the player has specified in the config menu.
		score.setStartingLevel(Integer.parseInt(propMgr.getProperty(Constants.PROP_STRTNG_LVL)));
		// Set the drop block timer according to what the current level is.
		timerDropBlock = new MyTimer(Constants.INTERVALDROPBLOCK - (score.getLevel() * 50));
		// Time elapsed will be derived from timeGameStart. Reset to zero here.
		timeElapsed = 0;
		// Set strAlrtMsg to nothing so as to ensure any lingering messages don't display over the gamefield (how annoying!).
		strAlrtMsg = "";
		// The next block will be used as the initial block. We can't check currentBlock.prepareForBirth because it's still null. This block will be copied into the currentBlock
		// onject when the birthing flag is checked and found to be true.
		nextBlock.prepareForBirth();
		// Set variable for Frames Per Second (development only).
//		int fpsCount = 0;
//		MyTimer fpsTimer = new MyTimer(1000);
		
		MusicClips.MUS_GAME[musicTrack].play();
		MusicClips.MUS_GAME[musicTrack].fadeIn(Float.valueOf(propMgr.getProperty(Constants.PROP_MUS_VOL)) / 10);

		// Repeat while game in progress
		while ((gameState == Constants.GameStates.PLAYGAME) || (gameState == Constants.GameStates.PAUSE)  || (gameState == Constants.GameStates.FASTHELP) || (gameState == Constants.GameStates.ENDGAME)) {

			// Show the Frames Per Second. This is currently only used for development.
//			if (fpsTimer.isIntervalReached()) {
//				strDebugMsg = "FPS: " + fpsCount;
//				fpsTimer.reset();
//				fpsCount = 0;
//			}

			// If the game state is PLAYGAME...
			if (gameState == Constants.GameStates.PLAYGAME) {
				if ((score.getLevel() / 2) > (musicTrack + Integer.parseInt(propMgr.getProperty("startingLevel")) / 2)) {
					MusicClips.MUS_GAME[musicTrack].fadeOut();
					MusicClips.MUS_GAME[musicTrack].stop();
					musicTrack++;
//					musicTrack = musicTrack % 7;
					if (musicTrack > 6) {
						musicTrack = 0;
					}
					MusicClips.MUS_GAME[musicTrack].play();
					MusicClips.MUS_GAME[musicTrack].fadeIn(Float.valueOf(propMgr.getProperty(Constants.PROP_MUS_VOL)) / 10);
				}
				
				// Clear any alert strings so that they don't splash over the gamefield.
				strAlrtMsg = "";
				// Make sure any bombs aren't paused.
				if (AbstractBomb.isBombDropping()) {
					bomb.resume();
				}
				// If there are rows that have been cleared then those rows need to be animated. There is a short delay in gameplay while it animates the blocks that
				// are to be deleted. The blocks are deleted only after that delay has elapsed.
				if (boolClearRowsAnim) {
					// If the clear row animation has completed two cycles...
					if (Screens.screenGame.getSpriteClearRow().getNumCycles() >= 2) {
						// ...reset the animation.
						Screens.screenGame.getSpriteClearRow().resetCycles();
						// Delete the cleared tiles from the stack.
						stack.deleteFullRows();
						// Set the clear row animation flag to false.
						boolClearRowsAnim = false;
					}
				} else {
					createNewBlock();
					processBlockDropInterval(boolClearRowsAnim);
					processPlayerMove();
					checkMoveIsValid();

					// If the player has specified in the config screen that s/he wants bombs activated, then do all the bomb processing.
					if (propMgr.getProperty(Constants.PROP_BMBS_ACTVTD).contentEquals(Constants.STR_TRUE)) {
						createNewBomb();
						processBombExplosion();
					}

					deleteExplodedTiles();
					
					// Check if the stacking flag for the current block is set...
					if (currentBlock.checkStackBlockNow()) {
						// ...and then we put that block on the stack, setting the clear rows flag if the row is full.
						boolClearRowsAnim = putBlockOnStack(boolClearRowsAnim);
						// Calculate the score.
						score.calcScore(stack.getRowsClearedThisTurn(), nextBlock.birthMeYet());
						// Reset the full row count (for rows that were full on this turn)
						stack.resetFullRowCount();
					}
					// When a blocks falls on the stack it doesn't automatically 'stack'. It sits there until the next drop interval is completed. This allows the player to do things
					// like move the block into a nook or cranny in the stack or just otherwise move it a bit more before it's stacked.
					// If the block is resting then set the defer stack block flag to true...
					else if (stack.checkIfBlockResting(currentBlock.getBlock())) {
						currentBlock.setDeferStackBlock(true);
					} else {
						// ...otherwise ensure it's false.
						currentBlock.setDeferStackBlock(false);
					}

				timeElapsed = System.currentTimeMillis() - timeGameStart - pauseTime; 
				}
			} else if (gameState == Constants.GameStates.PAUSE) {
				strAlrtMsg = "Game Paused.";
				if (AbstractBomb.isBombDropping()) {
					bomb.pause();
				}
			} else if (gameState == Constants.GameStates.FASTHELP) {
				if (AbstractBomb.isBombDropping()) {
					bomb.pause();
				}
			} else if (gameState == Constants.GameStates.ENDGAME) {
				if (AbstractBomb.isBombDropping()) {
					bomb.pause();
				}
			}

			drawGameScreen();

//			fpsCount++;
		}
		
		MusicClips.MUS_GAME[musicTrack].fadeOut();
		MusicClips.MUS_GAME[musicTrack].stop();
		timerNextBomb.close();
		timerDropBlock.close();
		// Ensure that AbstractBomb is not in dropping mode if game is restarted.
		AbstractBomb.stop();
		
	}

	
	// Set the frequency with which a new bomb is created.
	private void setBombFrequency() {
		// Set the bomb frequency according to what the player has specified in the config menu.
		final int bombFrequency = Integer.parseInt(propMgr.getProperty(Constants.PROP_BMB_FRQNCY));
		// The bomb frequency allows for a time range in which a new bomb is triggered. This switch sets the start and end interval times for bomb creation.
		switch(bombFrequency) {
			case 1:		intrvlNxtBmbMin = 0;		intrvlNxtBmbMax = 1;		break;	// Bomb creation is constant.
			case 2:		intrvlNxtBmbMin = 1000;		intrvlNxtBmbMax = 5000;		break;	// Bomb created every 1 to 5 seconds after the last. 
			case 3:		intrvlNxtBmbMin = 5000;		intrvlNxtBmbMax = 10000;	break;	// Bomb created every 5 to 10 seconds after the last.
			case 4:		intrvlNxtBmbMin = 10000;	intrvlNxtBmbMax = 20000;	break;	// Bomb created every 10 to 20 seconds after the last.	
			case 5:		intrvlNxtBmbMin = 20000;	intrvlNxtBmbMax = 30000;	break;	// Bomb created every 20 to 30 seconds after the last.
			default:	intrvlNxtBmbMin = 5000;		intrvlNxtBmbMax = 10000;			// Default is a bomb every 5 to 10 seconds.
						propMgr.setProperty(Constants.PROP_BMB_FRQNCY, "3");	break; 	// If we're here then the config property hasn't been set, so set it to the default.
		}
		// Set the next bomb timer according to the time range specified above.
		timerNextBomb = new MyTimer(intrvlNxtBmbMin, intrvlNxtBmbMax);
	}

	
	// Draw all elements of the game screen.	
	private void drawGameScreen() {
		// Call functions required for starting to draw the screen.
		Screens.screenGame.start();
		// Draw game field and base elements.
		Screens.screenGame.draw();
		// Draw any explosions taking place.
		Screens.screenGame.drawExplosion(stack);
		// If the next block is not due to be birthed then draw the current block (if it's due to be birthed it means the 'current block' is actually now part of the stack instead).
		if (!nextBlock.birthMeYet()) {
			Screens.screenGame.drawBlock(currentBlock);
		}
		// Draw the stack (of tiles).
		Screens.screenGame.drawStack(stack);
		// Check if a bomb is dropping and if so draw it.
		if (AbstractBomb.isBombDropping()) {
			Screens.screenGame.drawBomb(bomb.getX(), bomb.getY(), bomb.getBombType(), bomb.getExplodeY() - bomb.getY());
		}
		// If the player has specified in the config that s/he wants the next block to be displayed in a preview panel, draw it.
		if (propMgr.getProperty(Constants.PROP_DSP_NXT_BLCK).contentEquals(Constants.STR_TRUE)) {
			Screens.screenGame.drawBlockPreview(nextBlock.getAllBlocks(), nextBlock.getBlocknum());
		}
		// Draw all the player stats.
		Screens.screenGame.drawStats(stack, score, propMgr, timeElapsed);
		// If there is an alert message (e.g. Pause message), draw it.
		Screens.screenGame.drawAlertMsg(strAlrtMsg);
		if (gameState == Constants.GameStates.FASTHELP) {
			Screens.screenGame.drawHelp();
		}
		if (gameState == Constants.GameStates.ENDGAME) {
			Screens.screenGame.drawPrompToQuit();
		}
		// Call function required to complete drawing the screen.
		Screens.screenGame.finalise();
	}

	
	// If the block is to be put on the stack now, determined by if it is resting and the interval has been hit, then do it.
	private boolean putBlockOnStack(final boolean boolClearRowsAnim) {
		boolean boolClrRwsAnm = boolClearRowsAnim;
		
		// Put the current block on the stack.
		stack.putBlockOnStack(currentBlock.getBlock(), currentBlock.getBlocknum());
		// Play the sound clip for the block coming to rest on the stack or bottom of the gamefield.
		SoundClips.SND_RSTNG_BLCK.play();
		// The current block has now been 'stacked', so set the stacking flag to false.
		currentBlock.setStackBlockNow(false);
		// Get ready to birth the next block.
		nextBlock.prepareForBirth();

		// Check for full rows, delete the ones that are full.
		if (stack.getRowsClearedThisTurn() > 0) {
			// Play the sound clip for clearing a full row.
			SoundClips.SND_CLR_ROW.play();
			// If the total number of rows cleared is higher than the current level times 10, this indicates that the level should increase, so...
			if (stack.getTotRowsCleared() > (score.getLevel() * 10)) {
				// ...set it by passing the number of rows cleared to the score object, where it will calculate what level to set it to.
				score.setLevelByRows(stack.getTotRowsCleared());
				System.out.println(score.getLevel());
			}
			// Set the drop block timer interval based on the level. The speed increases by 50 ms per level.
			timerDropBlock.setInterval(Constants.INTERVALDROPBLOCK - (score.getLevel() * 50));
			// Set the clear rows animation to true. It's row clearin' time!
			boolClrRwsAnm = true;
			// Reset the animation for clearing the rows so that it starts from frame one.
			Screens.screenGame.getSpriteClearRow().resetCycles();
		}
		return boolClrRwsAnm;
	}


	// Set the block to what is the 'next block', generate a new 'next block', check if the new block is valid (i.e. not resting on anything as soon
	// as it hits the gamefield, which is an endgame state).
	private void createNewBlock() {
		// If the next block is due to be birthed...
		if (nextBlock.birthMeYet()) {
			// ...set the current block to the next block.
			currentBlock = nextBlock;
			// Create a new 'next block'.
			nextBlock = new Block();
			// Check that the current block is valid. It's possible that it's resting on top of the stack and sticking above the top row. This means end of game.
			if (!checkIfPropblockValid(stack.getStack(), currentBlock.getBlock())) {
				// Set the game state to postgame where we will tell the player the game is over and check for a high score.
				gameState = Constants.GameStates.POSTGAME;
			}
			// Reset the drop block timer (give the current block a chance to sit there for a second whilst the player registers it).
			timerDropBlock.reset();
		}
	}

	
	// Delete all the exploded tiles from the stack.
	private void deleteExplodedTiles() {
		// Check if the explosion animation has completed one full cycle. It's gotta look good on screen ;-)
		if (Screens.screenGame.getSpriteExplosion().getNumCycles() > 1) {
			// Tell the stack to delete the exploded tiles...
			stack.deleteExplodedTiles();
			// ...and then collapse any rows that are completely empty above this row.
			// NOTE: DOES NOT DELETE TILES WHERE A BLOCK HAS BEEN DROPPED IN THE EXPLODED AREA DURING THE EXPLOSION. THIS CAN LEAVE HANGING TILES AND MAY BE CONSIDERED A BUG DEPENDING
			// ON ALPHA TESTING FEEDBACK.
			stack.collapseEmptyRows();
			// Reset the explosion animation for the next time it happens.
			Screens.screenGame.getSpriteExplosion().resetCycles();
		}
	}

	
	// Check if the move made by the player is valid.
	private void checkMoveIsValid() {
		// If there has been a move made by the player and we're not going to stack the block yet...
		if ((blockMove != Constants.BlockMoves.NONE) && (!currentBlock.checkStackBlockNow())) {
			// ...and if the current block player move is valid...
			if (checkIfPropblockValid(stack.getStack(), currentBlock.getPropblock()) && (!isPropblockOnBomb(currentBlock.getPropblock(), bomb))) {
				// Tell the current block object that it's all good.
				currentBlock.confirmPropblock();
				// If the player just rotated the block then set the rotation set in the current block object to the next rotation in the queue.
				if (blockMove == Constants.BlockMoves.ROTATE) {
					currentBlock.incrementRotationset();
				}
				// snddrop.play(); // Sound effect for block dropping.
			}
			// Set blockMove to none because we've processed the player's move.
			blockMove = Constants.BlockMoves.NONE;
		}
	}


	// Check if interval elapsed. Block drops on row at the end of each interval unless the block is currently resting.
	private void processBlockDropInterval(final boolean boolClearRowsAnim) {
		// If the drop block interval is reached and no rows are being cleared (drops pause during this action)...
		if ((timerDropBlock.isIntervalReached()) && (!boolClearRowsAnim)) {
			// Reset the drop block timer.
			timerDropBlock.reset();
			// Check if we're deferring stacking the current block...
			if (currentBlock.checkDeferStackBlock()) {
				// ...and now make it that we're not deferring stack the current block.
				currentBlock.setStackBlockNow(true);
				currentBlock.setDeferStackBlock(false);
			} else {
				// If we're not deferring stacking the current block, move it down one row.
				currentBlock.moveBlockDown();
				
				// Check that this 'one row down move' is valid, and if so tell the current block object that it's all good.
				if (checkIfPropblockValid(stack.getStack(), currentBlock.getPropblock()) && (!isPropblockOnBomb(currentBlock.getPropblock(), bomb))) {
					currentBlock.confirmPropblock();
				}
			}
		}
	}

	
	// Create a new bomb.
	private void createNewBomb() {
		// When the next bomb timer has reached it's interval, this signals that it's time to create a new bomb.
		if (timerNextBomb.isTimerActive() && timerNextBomb.isIntervalReached()) {
			// Set up the random generator, because we want a random bomb type. No fun in having them all the same!
			final Random generator = new Random();
			// The starting position on the X-axis has to be no larger than STACKWIDTH, otherwise it will be out of bounds (illegal). 
			final int startX = generator.nextInt(Constants.STACKWIDTH);
			// Create a random bomb, choosing from one of three types: Fatboy, Leveller, and Piledriver.
			switch(generator.nextInt(3)) {
				case 0:	bomb = new BombFatboy(startX, 1000);		break;
				case 1: bomb = new BombLeveller(startX, 1000);		break;
				case 2:	bomb = new BombPiledriver(startX, 1000);	break;
				default:		break;
			}
			// Set flags in the NextBomb object that can be used to tell if the timer "no longer exists".
			timerNextBomb.close();
		}
	}

	
	// Take care of an exploding bomb.
	private void processBombExplosion() {
		if (AbstractBomb.isBombDropping()) {
			final boolean bombResting = stack.checkIfTileResting(bomb.getX(), bomb.getY());
			// If the bomb is resting on the stack or bottom of the gamefield then blow it up.
			if (bomb.explode(bombResting)) {
				// Restart the explosion animation.
				Screens.screenGame.getSpriteExplosion().resetCycles();
				// Get the stack object to start the explosion for the tiles that this bomb will destroy.  
				stack.explodeTileAtLocation(bomb.getMinExplodeX(), bomb.getMaxExplodeX(), bomb.getMinExplodeY(), bomb.getMaxExplodeY());
				// Play the explosion sound clip.
				SoundClips.SND_EXPLSN.play();
				// Reset the timer that determines when the next bomb will drop.
				timerNextBomb = new MyTimer(intrvlNxtBmbMin, intrvlNxtBmbMax);
				// Set flags in the bomb object that can be used to tell if the bomb "no longer exists".
				bomb.close();
			// If the bomb is a Leveller then delete the tile at the location it's dropped.
			} else if (bomb.getBombType().contentEquals(Constants.BOMBTYPE_LEVELLER)) {
				stack.deleteTileAtLocation(bomb.getX(), bomb.getY());
			}
		}
	}


	// Process blockMove as set during key press or if interval has been hit.
	private void processPlayerMove() {
		switch(blockMove) {
			case LEFT:		currentBlock.moveBlockLeft();				break;	// Move the block left.
			case RIGHT:		currentBlock.moveBlockRight();				break;	// Move the block right.
			case DOWN:		if (currentBlock.checkDeferStackBlock()) {			// If the block is resting on the stack or bottom of the gamefield then stack it.
								currentBlock.setDeferStackBlock(false);
								currentBlock.setStackBlockNow(true);
							} else {
								currentBlock.moveBlockDown();
							}
							break;
			case ROTATE:	currentBlock.rotateBlock();					break;	// Rotate the block.
			default:		break;
		}
	}

	
	// Check if the proposed block is a valid block. Return true if it is, false if it isn't.	
	public static boolean checkIfPropblockValid(final int stack[][], final int propblock[][]) {
		int tileX;
		int tileY;
		boolean boolValidBlock = true; 

		// Process each tile in the proposed block
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
			tileX = propblock[i][0];
			tileY = propblock[i][1];

			// Block position is false if a tile of the proposed block is outside the bounds of the stack.
			if ((tileX < 0) || (tileX >= Constants.STACKWIDTH) || (tileY < 0) || (tileY >= Constants.STACKHEIGHT)) {
				boolValidBlock = false;
			} else {
				// Block position is false if a tile of the proposed block is in the same position as a stack tile, unless the tile is empty or is exploding, 
				// or if a tile of the proposed block is in the same position as a bomb.
				if ((stack[tileX][tileY] != Constants.TILEEMPTY) && (stack[tileX][tileY] != Constants.TILEEXPLOSION)) {
					boolValidBlock = false;
				}
				
			}
		}
		
		return boolValidBlock;
	}
	
	
	// Check if the proposed block is on top of the bomb. This is not permitted so any move that results in this case will simply be reversed.
	public static boolean isPropblockOnBomb(final int propblock[][], final AbstractBomb bomb) {
		int tileX;
		int tileY;
		boolean boolPropblockOnBomb = false;
		
		for (int i = 0; i < Constants.TILESINBLOCK; i++)	{
			tileX = propblock[i][0];
			tileY = propblock[i][1];
			if (bomb != null) {
				if ((tileX == bomb.getX()) && (tileY == bomb.getY())) {
					boolPropblockOnBomb = true;
				}
			}
		}
		
		return boolPropblockOnBomb;
	}
	

	//
	// USER INPUT
	//
	
	@Override
	// Check for user key presses and process according to the current game state.
	public void keyPressed(final KeyEvent keyEvent) {
		final int keyCode = keyEvent.getKeyCode();
		switch (gameState) {
			case APPSTART:		gameState = Constants.GameStates.MAINMENU;		break;	// APPSTART state is followed by MAINMENU.
			case MAINMENU:		checkKeyPressedMM(keyCode);						break;	// MAINMENU state has numerous possible actions.
			case PLAYGAME:		checkKeyPressedInGame(keyCode);					break;	// PLAYGAME state has numerous possible actions.
			case POSTGAME:		checkKeyPressedPostgame(keyEvent, keyCode);		break;	// POSTGAME state has numerous possible actions.
			case PAUSE:			gameState = Constants.GameStates.PLAYGAME;		break;	// PAUSE state is only available during game play, followed by PLAYGAME.
			case FASTHELP:		gameState = Constants.GameStates.PLAYGAME;		break;	// PAUSE state is only available during game play, followed by PLAYGAME.
			case LADDER:		gameState = Constants.GameStates.MAINMENU;		break;	// LADDER state is followed by MAINMENU.
			case CONFIG:		checkKeyPressedConfig(keyCode);					break;	// CONFIG state is followed by MAINMENU.
																						// Config options currently only selectable by mouse, might change this in future. 
			case ENDGAME:		checkKeyPressedEndgame(keyCode);				break;
			default:			break;
		}

		keyEvent.consume();
	}

	// Check for user key presses and change the game state accordingly whilst in the Main Menu screen.
	private void checkKeyPressedEndgame(final int keyCode) {
		switch (keyCode) {
			case KeyEvent.VK_Y:		gameState = Constants.GameStates.MAINMENU;		break;	// F1 activates help dialog.
			case KeyEvent.VK_N:		gameState = Constants.GameStates.PLAYGAME;		break;	// F1 activates help dialog.
		}
	}

		// Check for user key presses and change the game state accordingly whilst in the Main Menu screen.
	private void checkKeyPressedMM(final int keyCode) {
		switch (keyCode) {
			case KeyEvent.VK_F1:	gameState = Constants.GameStates.FASTHELP;		break;	// F1 activates help dialog.
			case KeyEvent.VK_F2:	gameState = Constants.GameStates.PREGAME;		break; 	// S key starts the game.
			case KeyEvent.VK_F3:	gameState = Constants.GameStates.LADDER;		break;	// L key is for High score ladder.
			case KeyEvent.VK_F4:	gameState = Constants.GameStates.CONFIG;		break;	// C key is for Config menu.
			case KeyEvent.VK_F10:	gameState = Constants.GameStates.APPEND;		break;	// Q key is to quit the game application.
			default:				break;
		}
	}

	private void checkKeyPressedConfig(final int keyCode) {
		int pVal;

		switch (keyCode) {
			case KeyEvent.VK_F1:
				break;	// F1 activates help dialog.
			case KeyEvent.VK_F2:
				if (propMgr.getProperty(Constants.PROP_BMBS_ACTVTD).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_BMBS_ACTVTD, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_BMBS_ACTVTD, Constants.STR_TRUE); }
				break;
			case KeyEvent.VK_F3:
				if (propMgr.getProperty(Constants.PROP_DSP_NXT_BLCK).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_DSP_NXT_BLCK, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_DSP_NXT_BLCK, Constants.STR_TRUE); }
				break;
			case KeyEvent.VK_F4:
				if (propMgr.getProperty(Constants.PROP_DSP_TM_ELPSD).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_DSP_TM_ELPSD, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_DSP_TM_ELPSD, Constants.STR_TRUE); }
				break;
			case KeyEvent.VK_F5:
				if (propMgr.getProperty(Constants.PROP_INSTNT_DROP).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_INSTNT_DROP, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_INSTNT_DROP, Constants.STR_TRUE); }
				break;
			case KeyEvent.VK_F6:	
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_BMB_FRQNCY));
				pVal = (pVal % 6);
				propMgr.setProperty(Constants.PROP_BMB_FRQNCY, Integer.toString(pVal));
				break;
			case KeyEvent.VK_F7:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_STRTNG_LVL));
				pVal = (pVal % 6);
				propMgr.setProperty(Constants.PROP_STRTNG_LVL, Integer.toString(pVal));
				break;
			case KeyEvent.VK_F8:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_SFX_VOL));
				pVal = (pVal % 11);
				propMgr.setProperty(Constants.PROP_SFX_VOL, Integer.toString(pVal));
				break;
			case KeyEvent.VK_F9:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_MUS_VOL));
				pVal = (pVal % 11);
				propMgr.setProperty(Constants.PROP_MUS_VOL, Integer.toString(pVal));
				break;
			case KeyEvent.VK_F10:
				gameState = Constants.GameStates.MAINMENU;
				break; 	// S key starts the game.
			default:
				break;
		}
	}

	// Check for user key presses and change the game state accordingly whilst playing the game.
	private void checkKeyPressedInGame(final int keyCode) {
		switch (keyCode) {
			case KeyEvent.VK_UP:		blockMove = Constants.BlockMoves.ROTATE;	break;	// Up arrow key rotates the block.
			case KeyEvent.VK_LEFT:		blockMove = Constants.BlockMoves.LEFT;		break;	// Left arrow key moves the block left.
			case KeyEvent.VK_RIGHT:		blockMove = Constants.BlockMoves.RIGHT;		break;	// Right arrow key moves the block right.
			case KeyEvent.VK_DOWN:		blockMove = Constants.BlockMoves.DOWN;		break;	// Down arrow key moves the block down.
			case KeyEvent.VK_F1:		gameState = Constants.GameStates.FASTHELP;	break;	// F1 activates help dialog.
			case KeyEvent.VK_F9:		gameState = Constants.GameStates.PAUSE;		break;	// P key pauses the game.
			case KeyEvent.VK_F10:		gameState = Constants.GameStates.ENDGAME;	break;	// Q key is to quit the game application.
			default:					break;
		}
	}

	// Check for user key presses and change the game state accordingly straight after the game is finished.
	// This includes recognising keys for entering a name to go on the high score ladder.
	private void checkKeyPressedPostgame(final KeyEvent keyEvent, final int keyCode) {
		// Keys A-Z, 0-9 and Space are all allowed for a players name to be entered in the high score ladder.
		if (((keyCode >= KeyEvent.VK_A) && (keyCode <= KeyEvent.VK_Z)) ||
		    ((keyCode >= KeyEvent.VK_0) && (keyCode <= KeyEvent.VK_9)) ||
		    (keyCode == KeyEvent.VK_SPACE)) {		
			pName += keyEvent.getKeyChar();
		} else {
			switch (keyCode) {
				case KeyEvent.VK_BACK_SPACE:	if (pName.length() > 0) {
													pName = pName.substring(0, pName.length() - 1);	// Once a key is entered for the players name modify the container string pName.
												}
												break;
				case KeyEvent.VK_ENTER:			gameState = Constants.GameStates.LADDER;	break;	// Enter completes the player name entry and then changes the game state to LADDER. 
				default:						break;
			}
		}
	}

	@Override
	public void keyReleased(final KeyEvent keyEvent) {
		// TODO Auto-generated method stub
		keyEvent.consume();
	}

	@Override
	public void keyTyped(final KeyEvent keyEvent) {
		// TODO Auto-generated method stub
		keyEvent.consume();
	}

	@Override
	public void mouseClicked(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		mouseEvent.consume();
	}

	@Override
	public void mouseEntered(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		mouseEvent.consume();
	}

	@Override
	public void mouseExited(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		mouseEvent.consume();
	}

	@Override
	public void mousePressed(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		mouseX = mouseEvent.getX();
		mouseY = mouseEvent.getY();

		final int mouseCode = mouseEvent.getButton();
		switch (gameState) {
			case APPSTART:		gameState = Constants.GameStates.MAINMENU;		break;	// APPSTART state is followed by MAINMENU.
			case MAINMENU:		checkMousePosMM();								break;	// MAINMENU state has numerous possible actions.
			case PLAYGAME:		checkMousePosPG(mouseCode);						break;
			case ENDGAME:		checkMousePosEG(mouseCode);						break;
			case FASTHELP:		gameState = Constants.GameStates.PLAYGAME;		break;	// PAUSE state is followed by PLAYGAME.
			case PAUSE:			gameState = Constants.GameStates.PLAYGAME;		break;	// PAUSE state is followed by PLAYGAME.
			case LADDER:		gameState = Constants.GameStates.MAINMENU;		break;	// LADDER state is followed by MAINMENU.
			case CONFIG:		checkMousePosConfig();							break;	// CONFIG state has numerous possible actions.
			default:			break;
		}

		mouseEvent.consume();
	}

	private void checkMousePosEG(int mouseCode) {
		switch (Screens.screenGame.checkMousePos(mouseX, mouseY)) {
			case 10:	gameState = Constants.GameStates.MAINMENU;	break;	// Displays the ladder.
			case 11:	gameState = Constants.GameStates.PLAYGAME;	break;	// Quits the game application.
		}
//		if (mouseCode == MouseEvent.BUTTON1) {
//			gameState = Constants.GameStates.PLAYGAME;			// Left mouse button to continue playing the game.
//		} else if (mouseCode == MouseEvent.BUTTON3) {
//			gameState = Constants.GameStates.MAINMENU;			// Right mouse button to quit.
//		}
	}

	private void checkMousePosPG(int mouseCode) {
		switch (Screens.screenGame.checkMousePos(mouseX, mouseY)) {
			case 1:		gameState = Constants.GameStates.FASTHELP;	break;	// Quits the game application.
			case 2:		gameState = Constants.GameStates.PAUSE;		break;	// Displays the ladder.
			case 3:		gameState = Constants.GameStates.ENDGAME;	break;	// Quits the game application.
			default:	
				if (mouseCode == MouseEvent.BUTTON1) {
					blockMove = Constants.BlockMoves.DOWN;				// Left mouse button click moves the block down.
				} else if (mouseCode == MouseEvent.BUTTON3) {
					blockMove = Constants.BlockMoves.ROTATE;			// Right mouse button click rotates the block.
				}
				break;
		}
	}
	
	private void checkMousePosMM() {
		switch (Screens.screenMainMenu.checkMousePos(mouseX, mouseY)) {
			case 1:		gameState = Constants.GameStates.PREGAME;	break;	// Starts the game.
			case 2:		gameState = Constants.GameStates.CONFIG;	break;	// Displays the Config menu.
			case 3:		gameState = Constants.GameStates.LADDER;	break;	// Displays the ladder.
			case 4:		gameState = Constants.GameStates.APPEND;	break;	// Quits the game application.
			default:	break;
		}
	}

	private void checkMousePosConfig() {
		int pVal;
		
		switch (Screens.screenConfig.checkMousePos(mouseX, mouseY)) {
			case 1:
				if (propMgr.getProperty(Constants.PROP_BMBS_ACTVTD).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_BMBS_ACTVTD, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_BMBS_ACTVTD, Constants.STR_TRUE); }
				break;
			case 2:
				if (propMgr.getProperty(Constants.PROP_DSP_NXT_BLCK).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_DSP_NXT_BLCK, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_DSP_NXT_BLCK, Constants.STR_TRUE); }
				break;
			case 3:
				if (propMgr.getProperty(Constants.PROP_DSP_TM_ELPSD).contentEquals(Constants.STR_TRUE))
					{ propMgr.setProperty(Constants.PROP_DSP_TM_ELPSD, Constants.STR_FALSE); }
				else
					{ propMgr.setProperty(Constants.PROP_DSP_TM_ELPSD, Constants.STR_TRUE); }
				break;
			// Decrease bomb frequency
			case 10:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_BMB_FRQNCY));
				if (pVal <= 1) {
					pVal = 1;
				} else if (pVal > 5) {
					pVal = 5;
				} else {
					pVal--;
				}
				propMgr.setProperty(Constants.PROP_BMB_FRQNCY, Integer.toString(pVal));
				break;
			// Increase bomb frequency
			case 11:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_BMB_FRQNCY));
				if (pVal < 1) {
					pVal = 1;
				} else if (pVal >= 5) {
					pVal = 5;
				} else {
					pVal++;
				}
				propMgr.setProperty(Constants.PROP_BMB_FRQNCY, Integer.toString(pVal));
				break;
			// Decrease starting level
			case 12:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_STRTNG_LVL));
				if (pVal <= 1) {
					pVal = 1;
				} else if (pVal > 20) {
					pVal = 20;
				} else {
					pVal--;
				}
				propMgr.setProperty(Constants.PROP_STRTNG_LVL, Integer.toString(pVal));
				break;
			// Increase starting level
			case 13:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_STRTNG_LVL));
				if (pVal < 0) {
					pVal = 0;
				} else if (pVal >= 20) {
					pVal = 20;
				} else {
					pVal++;
				}
				propMgr.setProperty(Constants.PROP_STRTNG_LVL, Integer.toString(pVal));
				break;
			case 14:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_SFX_VOL));
				if (pVal <= 0) {
					pVal = 0;
				} else if (pVal > 10) {
					pVal = 10;
				} else {
					pVal--;
				}
				propMgr.setProperty(Constants.PROP_SFX_VOL, Integer.toString(pVal));
				setSfxVolumes();
				break;
			// Increase starting level
			case 15:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_SFX_VOL));
				if (pVal < 0) {
					pVal = 0;
				} else if (pVal >= 10) {
					pVal = 10;
				} else {
					pVal++;
				}
				propMgr.setProperty(Constants.PROP_SFX_VOL, Integer.toString(pVal));
				setSfxVolumes();
				break;
			case 16:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_MUS_VOL));
				if (pVal <= 0) {
					pVal = 0;
				} else if (pVal > 10) {
					pVal = 10;
				} else {
					pVal--;
				}
				propMgr.setProperty(Constants.PROP_MUS_VOL, Integer.toString(pVal));
				setMusVolume();
				break;
			// Increase starting level
			case 17:
				pVal = Integer.parseInt(propMgr.getProperty(Constants.PROP_MUS_VOL));
				if (pVal < 1) {
					pVal = 1;
				} else if (pVal >= 10) {
					pVal = 10;
				} else {
					pVal++;
				}
				propMgr.setProperty(Constants.PROP_MUS_VOL, Integer.toString(pVal));
				setMusVolume();
				break;
			case 99:
				gameState = Constants.GameStates.MAINMENU;
				break;
			default:
				break;
		}
		SoundClips.SND_RSTNG_BLCK.play();

	}

	@Override
	public void mouseReleased(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		mouseEvent.consume();
	}

	@Override
	public void mouseDragged(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		mouseEvent.consume();
	}

	@Override
	public void mouseMoved(final MouseEvent mouseEvent) {
		// TODO Auto-generated method stub
		int mousePrevX = 0;
		mousePrevX = mouseX;
		
		mouseX = mouseEvent.getX();
		mouseY = mouseEvent.getY();
		
		if (gameState.equals(Constants.GameStates.PLAYGAME)) {
			if (mouseX + 10 < mousePrevX) {
				blockMove = Constants.BlockMoves.LEFT;
				mousePrevX = mouseX;
			} else if (mouseX - 10 > mousePrevX) {
				blockMove = Constants.BlockMoves.RIGHT;
				mousePrevX = mouseX;
			}
		}

		mouseEvent.consume();
	}

}
