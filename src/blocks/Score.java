package blocks;

public class Score {
	
	private transient int level;
	private transient int theScore;
	private transient int streakCount;
	private transient boolean streak;
	
	
	public Score() {
		level = 1;
		theScore = 0;
		streakCount = 0;
		streak = false;
	}

	public void setStartingLevel(final int level) {
		this.level = level;
	}
	
	public void setLevelByRows(final int rowsCleared) {
		level = ((rowsCleared / 10) + 1);
	}

	public int getLevel() {
		return level;
	}
	
	public int getScore() {
		return theScore;
	}
	
	public int getStreak() {
		return streakCount;
	}
	
	public void calcScore(final int rowsCleared, final boolean newBlock) {
		if (newBlock) {
			theScore = theScore + (4 * level); // 4 represents 1 pt for each square in the block
			
			if (rowsCleared == 4) {
				streak = true;
				streakCount += 4;
			} else if ((rowsCleared > 0) && (rowsCleared < 4)) {
				streak = false;
				streakCount = 0;
			}
			
			if (rowsCleared > 0) {
				theScore += ((rowsCleared * 10) * level);
				if (streak) {
					theScore += (streakCount * level);
				}
			}
		}
		
		
	}
	
}
