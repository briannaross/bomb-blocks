package blocks;

import javax.sound.sampled.*;
import java.io.*;

public class AudioManager {

    private transient AudioInputStream sample;			    // The source for audio data.
    private transient Clip clip;							// Sound clip property is read-only.
    private transient String filename = "";					// Filename of the sound clip to load.
    private transient int repeat = 0;						// Repeat property used to play sound multiple times
    private transient boolean looping = false;				// Looping property for continuous playback.


    // Constructor
    public AudioManager() {
        // Create a sound buffer
        try {
            clip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
        	e.printStackTrace();
        }
    }

    // Overloaded constructor, takes an audio clip filename as a parameter.
    public AudioManager(final String fileAudioClip) {
        this();						// Call the default constructor first.
//        load(strLocAudioPath);		// Load the audio file.
    	try {
        	sample = AudioSystem.getAudioInputStream(this.getClass().getResource(fileAudioClip)); 
            clip.open(sample);
        } catch (IOException e) {
        	e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
        	e.printStackTrace();
        } catch (LineUnavailableException e) {
        	e.printStackTrace();
        }
    }

    public Clip getClip() {
    	return clip;
    }

    // Load the audio file.
    public boolean load(final String fileAudioClip) {
    	boolean boolLoaded;
    	
    	try {
        	sample = AudioSystem.getAudioInputStream(this.getClass().getResource(fileAudioClip)); 
            clip.open(sample);
            boolLoaded = true;
        } catch (IOException e) {
        	boolLoaded = false;
        } catch (UnsupportedAudioFileException e) {
        	boolLoaded = false;
        } catch (LineUnavailableException e) {
        	boolLoaded = false;
        }
    	
    	return boolLoaded;
    }

	public void play() {
        if (!isLoaded()) {	//exit if the sample hasn't been loaded
        	return;	
        }

        clip.setFramePosition(0);	//reset the sound clip

        //play sample with optional looping
        if (looping) {
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } else {
            clip.loop(repeat);
        }
    }

    public void stop() {
        clip.stop();
    }

    // Set whether the clip should loop.
    public void setLooping(final boolean loop) {
    	looping = loop;
    }

    // Get the current looping status.
    public boolean isRunning() {
    	return clip.isRunning();
    }

    // Get the current looping status.
    public boolean isLooping() {
    	return looping;
    }

    // Set the number of time to repeat the clip.
    public void setRepeat(final int rpt) {
    	repeat = rpt;
    }

    // Get the number of time the clip is to repeat.
    public int getRepeat() {
    	return repeat;
    }

    // Set the filename.
    public void setFilename(final String fName) {
    	filename = fName;
    }

    // Get the filename.
    public String getFilename() {
    	return filename;
    }

    // Verify when sample is ready
    public boolean isLoaded() {
        return (boolean)(sample != null);
    }

    // Set the volume on this clip.
    public void setVolume(int vol) {
    	float newVol;
    	final FloatControl volume = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
    	float range = volume.getMaximum() - volume.getMinimum();
    	float increment = range / 20f;

    	if (vol > 0) {
    		newVol = volume.getMinimum() + (increment * 10) + (increment * vol);
    	} else {
    		newVol = volume.getMinimum();
    	}

   		volume.setValue(newVol);
    }

}