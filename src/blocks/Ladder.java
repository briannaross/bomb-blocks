package blocks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

// Using SQLite as I may be expanding this so that it takes in more data than just the player name, score, and date. It could be
// used for determining performance with different configurations, and it might be used for a global player ladder if ever this
// is ported to a web application.

public class Ladder {

	private transient final Connection con;
	private transient final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	
	public Ladder() throws SQLException, ClassNotFoundException, IOException, ParseException {
		final File file = new File(Constants.PATH_LADDER_DATA);
		if (!file.isFile())
			{ file.createNewFile();	}

		Class.forName("org.sqlite.JDBC");

		con = DriverManager.getConnection("jdbc:sqlite:mydb.db");

		final Statement stmt = con.createStatement();
		stmt.executeUpdate("drop table if exists ladder;");
		stmt.executeUpdate("create table ladder(name varchar(30), score int(11), scoredate integer);");

		final PreparedStatement prepStmt = con.prepareStatement("insert into ladder values(?,?,?);");

	    //final Scanner scanner = new Scanner(file).useDelimiter(";");
		final Scanner scanner = new Scanner(file);
		scanner.useDelimiter(";");

	    while (scanner.hasNext()) {
	    	prepStmt.setString(1, scanner.next());
	    	prepStmt.setInt(2, scanner.nextInt());
			prepStmt.setLong(3, scanner.nextLong());
			prepStmt.execute();
			if (scanner.hasNextLine()) {
				scanner.nextLine();
			}
	    }
	    
	    scanner.close();
	}

	
	public String[][] getLadder() throws SQLException {
		String[][] ladder = new String[10][3];
		Date resultdate;
		int resultIndex = 0;

		final PreparedStatement prepStmt = con.prepareStatement("SELECT * FROM ladder ORDER BY score DESC LIMIT 10");
		final ResultSet resultSet = prepStmt.executeQuery();

		while(resultSet.next()) {
			ladder[resultIndex][0] = resultSet.getString("name");
			ladder[resultIndex][1] = resultSet.getString("score");
			
			resultdate = new Date(resultSet.getLong("scoredate"));
			ladder[resultIndex][2] = dateFormat.format(resultdate);
			resultIndex++;
		}

		resultSet.close();

		return ladder.clone();
	}

	
	public boolean checkForHighScore(final int score) throws SQLException {
		boolean boolTopTen = false;
		
		final PreparedStatement statement = con.prepareStatement("SELECT count(*) c FROM ladder WHERE score >= " + score);
		final ResultSet resultSet = statement.executeQuery();

		if (resultSet.next()) {
			if (resultSet.getInt("c") >= 10) {
				boolTopTen = false;
			} else {
				boolTopTen = true;
			}
		}
		
		resultSet.close();
		
		return boolTopTen;
	}

	
	public void setNewHighScore(final String pName, final int score) throws Exception {
		final PreparedStatement prepStmt = con.prepareStatement("INSERT INTO ladder (name, score, scoredate) VALUES('" + pName + "', "	+ score + ", " + System.currentTimeMillis() + ")");
		prepStmt.executeUpdate();
	}

	
	public void saveLadder() throws SQLException, IOException {
		final PrintWriter out = new PrintWriter(new FileWriter(Constants.PATH_LADDER_DATA));

		final PreparedStatement statement = con.prepareStatement("SELECT * FROM ladder");
		final ResultSet resultSet = statement.executeQuery();

		while(resultSet.next()) {
			out.println(resultSet.getString("name") + ";" + resultSet.getInt("score")  + ";" + resultSet.getLong("scoredate") + ";");
		}
		
		resultSet.close();
		out.close();
	}
}
