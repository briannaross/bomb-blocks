package blocks;

import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.Format;
import javax.media.MediaLocator;
import javax.media.Player;
import javax.media.PlugInManager;
import javax.media.bean.playerbean.MediaPlayer;
import javax.media.format.AudioFormat;


public class MediaManager implements ControllerListener {

	private MediaPlayer player;

	public MediaManager() {
		Format input1 = new AudioFormat(AudioFormat.MPEGLAYER3);
		Format input2 = new AudioFormat(AudioFormat.MPEG);
		Format output = new AudioFormat(AudioFormat.LINEAR);
		PlugInManager.addPlugIn("com.sun.media.codec.audio.mp3.JavaDecoder", new Format[]{input1, input2}, new Format[]{output}, PlugInManager.CODEC);
	}
	
	public void load(String filename) {
		try{
			player = new javax.media.bean.playerbean.MediaPlayer();
			player.setMediaLocator(new MediaLocator("file:music//" + filename));
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		player.addControllerListener(this);
		player.realize();
	}
	
	public void play() {
		player.start();
		player.setPlaybackLoop(true);
		
	}

	public void stop() {
		player.stop();
	}

	public float getVolume() {
		return player.getGainControl().getLevel();
	}
	
	public void setVolume(float volume) {
		player.getGainControl().setLevel(volume);
	}
	
	public void fadeIn(final float targetVol) {
//		System.out.println(targetVol);
		float increment = targetVol / 20;
		float volume = 0;
		setVolume(volume);

		if (!isRunning()) {
			play();
		}
		
		while(getVolume() < targetVol) {
			setVolume(volume);
			volume = volume + increment;
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	public void fadeOut() {
		float volume = getVolume();
		float increment = volume / 20;
		
		if (!isRunning()) {
			play();
		}
		
		while(getVolume() > 0) {
			volume = volume - increment;
			setVolume(volume);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		stop();

	}

	@Override
	public void controllerUpdate(ControllerEvent cEvent) {
		// TODO Auto-generated method stub
		
		
	}

	public boolean isRunning() {
		boolean boolIsRunning;
		
		if (player.getState() == Player.Started) {
			boolIsRunning = true;
		} else {
			boolIsRunning = false;
		}
		
		return boolIsRunning;
	}
}

